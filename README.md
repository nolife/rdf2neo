# RDF2NEO #

This tool extracts graphs from RDF files and loads those into the Neo4j database. It supports a variety of RDF file formats, groups triples to create graphs, links similar triple nodes together thus you can see the complete and connected map of loaded files. The tool uses memory cache, internal Lucene or external Solr to index triples and can write its output either directly to Neo4j data directory or it may connect to your running Neo4j database and update that, so you may see the results while the tool is still running.

Author **Sergei Abramov**

Email **cftp@coldcore.com** 

License **LGPL**

![RDF2NEO_overview.png](https://bitbucket.org/repo/yX8zpn/images/1305221554-RDF2NEO_overview.png)

![RDF2NEO_output_1.png](https://bitbucket.org/repo/yX8zpn/images/362080609-RDF2NEO_output_1.png)
![RDF2NEO_output_2.png](https://bitbucket.org/repo/yX8zpn/images/2562242506-RDF2NEO_output_2.png)

## What you need to run this tool ##
* Java 7
* Solr 5 *(optional)*
* Neo4j 2.2.1 *(optional)*
* Maven 3

### How to install Java ###
1. Download from https://java.com/en/download
2. Install
3. Check with *java -version* in the terminal

### How to install Solr ###
1. Download from http://lucene.apache.org/solr/downloads.html
2. Unpack into your *SOLR_HOME* directory
3. Download *rdf2neo-solr-core-1.0.zip* from the project download section https://bitbucket.org/nolife/rdf2neo/downloads and unzip it
4. Go into *SOLR_HOME/server/solr/* directory and copy *rdf2neo-core* directory (step 3) into it
5. Go into *SOLR_HOME/bin* directory and start Solr from the terminal as *solr start*
6. Check by pointing your browser to http://localhost:8983/solr/#/rdf2neo-core

### How to install Neo4j ###
1. Download from https://neo4j.com/download-center
2. Install / Unpack into your *NEO4J_HOME* directory
3. Go into *NEO4J_HOME/bin* directory and start Neo4j from the terminal as *neo4j start*
4. Check by pointing your browser to http://localhost:7474/browser

### How to install Maven ###
1. Download from https://maven.apache.org/download.cgi
2. Install
3. Check with *mvn -version* in the terminal

## How to run the tool ##
* GIT clone the project from the Bitbucket into your *RDF2NEO* directory or download the repository
 
* Edit *config.properties* in the *RDF2NEO/src/main/resources* folder and change it according to your environment
* Go into *RDF2NEO* derectory and run from the terminal as *mvn install exec:java*
* Sit back and enjoy the ride as it may take a while

## Where to get dataset files ##
* [DBPedia](http://dbpedia.org) download section [Downloads2014](http://wiki.dbpedia.org/Downloads2014)
* [Datahub.io](http://datahub.io) contains links to interesting RDF such as [Nobel Prizes](http://datahub.io/dataset/nobelprizes)
* *RDF2NEO/src/main/resources* directory contains sample files

## Notes ##
* When processing large dataset files do so in stages, one component at a time by setting components' enabled flag to false. Always take backup of the index directory located in the cache data directory (or the Solr core), restore it if *index2neo4j* component terminates abnormally.

## Bugs ##
* Large dataset link nodes time is estimated incorrectly when using *same-anon* mode. Links usually finish when the counter reaches 50%, thus do not get frightened by unrealistic estimated time. 
 
## Change log ##
### v1.2 ###
* Support for Neo4j 3.5.5
* Deprecated Lucene index (old version is not working)

### v1.1 ###
* Big fix

### v1.0 ###
* Supports RDF file formats: NTriples (*.nt), Turtle (*.ttl), RDF/XML (*.xml)
* Supports nodes types: URI, Literals, Anon
* Optional external software: Solr 5, Neo4j 2.2.1
* Triples can be grouped by subject nodes
* Triples subject and object nodes can be linked together
