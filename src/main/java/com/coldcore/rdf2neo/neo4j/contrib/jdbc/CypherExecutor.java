package com.coldcore.rdf2neo.neo4j.contrib.jdbc;

import java.util.Iterator;
import java.util.Map;

/**
 * GitHub project:
 *    https://github.com/neo4j-contrib/developer-resources/tree/gh-pages/language-guides/java/jdbc/
 *
 * @author Michael Hunger @since 22.10.13
 */
public interface CypherExecutor {
  Iterator<Map<String,Object>> query(String statement, Map<String,Object> params);
}