package com.coldcore.rdf2neo
package tdb2index

import akka.actor._
import java.io.File
import com.coldcore.misc5.CFile
import com.coldcore.rdf2neo.actors.ProgressActor
import com.hp.hpl.jena.query.{ResultSet, Dataset}
import akka.actor.SupervisorStrategy.Escalate
import com.coldcore.rdf2neo.toolkit.tdb.TdbToolkit
import com.coldcore.rdf2neo.toolkit.index._
import org.apache.solr.client.solrj.impl.HttpSolrClient
import java.util.concurrent.atomic.AtomicLong
import com.coldcore.rdf2neo.toolkit.{CacheDir, ItemsInQueue, Convert, XTriple}
import scala.collection.mutable.ListBuffer
import java.text.DecimalFormat
import scala.Some
import akka.actor.OneForOneStrategy
import akka.routing.RoundRobinPool

sealed trait IndexMode
case object ClearIndexMode extends IndexMode
case object ErrorIndexMode extends IndexMode
case object AppendIndexMode extends IndexMode

case class Config(props: Map[String,String]) {
  def prop(key: String, pfx: String = "tdb2index"): String = {
    val p = props.getOrElse(s"$pfx.$key", "") or props.getOrElse(key, "")
    if (p.trim.isEmpty) throw new ConfigParamNotFoundException(s"$pfx.$key")
    p
  }

  def solrurl = prop("solr-url")
  def lucenedir = CacheDir.getCacheFolder("lucene-index")
  val tdbdir = new File(prop("tdb-dir"))
  val modelName =
    prop("tdb-model-name").toLowerCase match {
      case "default" => ""
      case mname => mname
    }
  val indexMode: IndexMode =
    prop("index-mode").toLowerCase match {
      case "append" => AppendIndexMode
      case "error" => ErrorIndexMode
      case "clear" => ClearIndexMode
      case mode => throw new InvalidConfigParamException("index-mode", mode)
    }
  val indexClientMode: IndexClientMode =
    prop("index-client-mode").toLowerCase match {
      case "solr" => SolrIndexClientMode
      case "lucene" => LuceneIndexClientMode
      case "memdb" => MemdbIndexClientMode
      case mode => throw new InvalidConfigParamException("index-client-mode", mode)
    }
  val defaultModelName = modelName.isEmpty ? true | false
  val nWriters = indexClientMode match {
    case LuceneIndexClientMode => 1
    case MemdbIndexClientMode => 1
    case _ => prop("n-writers").toInt
  }
  val limitTriples = try { prop("limit-triples").toLong } catch { case _: Throwable => Long.MaxValue }

  var dataset: Dataset = _
  var indexClientFactory: IndexClientFactory = _
}

class IndexClientFactory(config: Config) {
  val solrClient = // single instance
    if (config.indexClientMode == SolrIndexClientMode) {
      val httpClient = new HttpSolrClient(config.solrurl)
      Some(new SolrIndexClient(httpClient))
    } else None

  val luceneClient = // single instance
    if (config.indexClientMode == LuceneIndexClientMode) Some(new LuceneIndexClient(config.lucenedir)) else None

  def getClient: IndexClient =
    config.indexClientMode match {
      case SolrIndexClientMode => solrClient.get
      case LuceneIndexClientMode => luceneClient.get
      case MemdbIndexClientMode => new MemdbIndexClient
    }
}

object MasterActor {
  case class StartIN(config: Config)
  case object StopIN
  case class StopOUT(result: Boolean, loaded: Long, total: Long)
}

class MasterActor extends Actor {
  import MasterActor._

  val loader = context.actorOf(Props[TdbToIndexLoadActor], name = "tdb2index.TdbToIndexLoadActor")

  class State(val config: Config) {
    //println(s"Source TDB ${config.tdbdir.getAbsolutePath}")
    //println(s"Target Solr ${config.solrurl}")
    //println(s"TDB model ${config.defaultModelName ? "Default" | config.modelName}")

    if (config.indexMode == ClearIndexMode && config.indexClientMode == LuceneIndexClientMode && config.lucenedir.exists) {
      println("Deleting Lucene data directory")
      CacheDir.deleteCacheFolder("lucene-index")
    }

    if (config.indexMode == ClearIndexMode && config.indexClientMode == MemdbIndexClientMode) {
      println("Deleting index cache files")
      CacheDir.deleteCacheFolder("memdb-index")
    }

    val dataset = TdbToolkit.openDataset(config.tdbdir)
    config.dataset = dataset

    val indexClientFactory = new IndexClientFactory(config)
    val indexClient = indexClientFactory.getClient
    config.indexClientFactory = indexClientFactory

    var result: Boolean = false
    var (totalTriples, loadedTriples) = (0L, 0L)
  }

  var state: State = _

  def assertIndexMode: Boolean = {
    val n = state.indexClient.countRecords
    println(s"Index contains $n records")
    state.config.indexMode match {
      case ClearIndexMode if n > 0 =>
        println("Clearing index")
        state.indexClient.clear
        true
      case ErrorIndexMode if n > 0 =>
        println("[ERROR] Index contains data")
        self ! MasterActor.StopIN
        false
      case _ => true
    }
  }

  override def supervisorStrategy: SupervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 0) {
      case _ =>
        closeResources
        Escalate
    }

  def closeResources =
    try {
      println("Closing TDB dataset...")
      TdbToolkit.closeDataset(state.dataset)
    } catch { case t: Throwable => t.printStackTrace }

  def receive = {
    case StartIN(config) =>
      state = new State(config)
      if (assertIndexMode) loader ! TdbToIndexLoadActor.StartIN(state.config)

    case TdbToIndexLoadActor.StopOUT(total, loaded) =>
      val n = state.indexClient.countTriples
      println(s"Index contains $n triples")
      if (n > 0) {
        state.totalTriples = total
        state.loadedTriples = loaded
        state.result = true
      }
      self ! StopIN

    case StopIN =>
      closeResources
      context.stop(self)
      context.parent ! StopOUT(state.result, state.loadedTriples, state.totalTriples)
  }

}

object TdbToIndexLoadActor {
  case class StartIN(config: Config)
  case class StopOUT(total: Long, loaded: Long)
}

class TdbToIndexLoadActor extends Actor with ItemsInQueue {
  import TdbToIndexLoadActor._

  class State(val config: Config) {
    val indexClient = config.indexClientFactory.getClient
    val dataset = config.dataset
    val total = TdbToolkit.countTriples(dataset, config.modelName)
    var rs: ResultSet = TdbToolkit.readTriples(dataset, config.modelName)
    var (loaded, processed) = (0L, 0L)
    val stats = new Stats
    def everythingRead = state.processed >= state.total || nothingRead
    var nothingRead = false
  }

  var state: State = _

  class Stats {
    var (startTime, stopTime) = (0L, 0L)
    def start = startTime = System.currentTimeMillis
    def stop = stopTime = System.currentTimeMillis
    def executingTime = Convert.durationWords(stopTime-startTime)

    val df = new DecimalFormat("0.000")
    val (readTimePerTriple, writeTimePerTriple, bufferSize) = (new ListBuffer[Long], new ListBuffer[Long], 1000)
    def tripleRead(time: Long) = if (readTimePerTriple.size < bufferSize) readTimePerTriple += time
    def tripleWrite(time: Long) = if (writeTimePerTriple.size < bufferSize) writeTimePerTriple += time
    val avg = (x: ListBuffer[Long]) => if (x.nonEmpty) df.format(x.sum.toDouble/x.size.toDouble/1000d)+"s" else "?"
    def avgTripleReadTime = avg(readTimePerTriple)
    def avgTripleWriteTime = avg(writeTimePerTriple)
  }

  val progress = context.actorOf(Props[ProgressActor], name = "tdb2index.ProgressActor")
  var reader: ActorRef = _
  var writer: ActorRef = _

  def createReader =
    reader = context.actorOf(Props(new ReadActor(state.rs, state.config)),
      name = "tdb2index.ReadActor")

  def createWriter =
    writer = context.actorOf(Props(new WriteActor(state.config)).
      withRouter(RoundRobinPool(state.config.nWriters)), name = "tdb2index.WriteActor")

  override def supervisorStrategy: SupervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 0) { case _ => Escalate }

  def toggleReader =
    if (!state.everythingRead)
      if (hghItemsInQueue) reader ! ReadActor.PauseIN
      else if (lowItemsInQueue) reader ! ReadActor.ResumeIN

  def receive = {
    case StartIN(config) =>
      println(s"Loading data from TDB into index")
      state = new State(config)
      println(s"TDB contains ${state.total} triples")
      progress ! ProgressActor.StartIN(state.total)
      state.stats.start
      createWriter
      createReader

    case ReadActor.TripleOUT(triple, time) =>
      incItemsInQueue
      toggleReader
      if (triple.isDefined) state.stats.tripleRead(time)
      writer ! WriteActor.TripleIN(triple)

    case ReadActor.NoDataOUT =>
      state.nothingRead = true
      if (noItemsInQueue) self ! WriteActor.IdleOUT(0, time = 0) // simulate IdleOUT

    case WriteActor.IdleOUT(n, time) =>
      decItemsInQueue
      state.processed += 1
      state.loaded += n
      toggleReader
      if (n > 0) {
        state.stats.tripleWrite(time)
        progress ! ProgressActor.UpdateIN(n)
      }
      if (state.everythingRead && noItemsInQueue) progress ! ProgressActor.StopIN

    case ProgressActor.StopOUT =>
      state.stats.stop
      println("Index commit...")
      state.indexClient.commit
      state.indexClient.optimize
      println(s"Loaded ${state.loaded} triples of ${state.total} in ${state.stats.executingTime}")
      println(s"Avg triple read ${state.stats.avgTripleReadTime} and write ${state.stats.avgTripleWriteTime}")
      context.parent ! StopOUT(state.total, state.loaded)
  }

}

object ReadActor {
  case object LoopIN
  case object PauseIN
  case object ResumeIN
  case class TripleOUT(triple: Option[XTriple], time: Long)
  case object NoDataOUT
}

class ReadActor(rs: ResultSet, config: Config) extends Actor {
  import ReadActor._

  var (count, limit) = (0L, config.limitTriples)
  var paused = false

  self ! LoopIN

  def receive = {
    case LoopIN if count >= limit =>
      paused = true
      context.parent ! NoDataOUT

    case LoopIN if !paused && rs.hasNext =>
      val start = System.currentTimeMillis
      TdbToolkit.makeTriple(rs.next) match {
        case Some(triple) =>
          count += 1
          context.parent ! TripleOUT(Some(triple), time = System.currentTimeMillis-start)
        case _ =>
          context.parent ! TripleOUT(None, time = 0)
      }
      self ! LoopIN

    case PauseIN =>
      paused = true

    case ResumeIN if paused =>
      paused = false
      self ! LoopIN
  }

}

object WriteActor {
  case class TripleIN(triple: Option[XTriple])
  case class IdleOUT(n: Int, time: Long)

  var nextId = new AtomicLong(System.currentTimeMillis)
}

class WriteActor(config: Config) extends Actor {
  import WriteActor._

  val indexClient = config.indexClientFactory.getClient

  def addTriple(triple: XTriple) {
    triple.id =
      if (config.indexMode == AppendIndexMode)
        indexClient.lookupTripleId(triple) match {
          case Some(x) => x
          case None => nextId.getAndIncrement
        } else nextId.getAndIncrement
    indexClient.addTriple(triple, commit = false)
  }

  def receive = {
    case TripleIN(Some(triple)) =>
      val start = System.currentTimeMillis
      addTriple(triple)
      sender ! IdleOUT(1, time = System.currentTimeMillis-start)

    case TripleIN(None) =>
      sender ! IdleOUT(0, time = 0)
  }

}
