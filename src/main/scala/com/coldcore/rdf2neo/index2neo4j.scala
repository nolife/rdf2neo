package com.coldcore.rdf2neo
package index2neo4j

import java.text.DecimalFormat
import akka.actor._
import akka.actor.SupervisorStrategy.Escalate
import com.coldcore.rdf2neo.actors.ProgressActor
import org.apache.solr.client.solrj.impl.HttpSolrClient
import com.coldcore.rdf2neo.toolkit.index._
import com.coldcore.rdf2neo.toolkit.neo4j._
import com.coldcore.rdf2neo.toolkit._
import scala.collection.JavaConverters._
import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer
import com.coldcore.rdf2neo.neo4j.contrib.jdbc.JdbcCypherExecutor
import java.io.File
import com.coldcore.misc5.CFile
import com.coldcore.rdf2neo.toolkit.XTriple
import akka.routing.Broadcast
import akka.actor.OneForOneStrategy
import com.coldcore.rdf2neo.toolkit.PlainLiteralNode
import com.coldcore.rdf2neo.toolkit.UriNode
import akka.routing.RoundRobinPool
import com.coldcore.rdf2neo.toolkit.Shard

sealed trait DbMode
case object ClearDbMode extends DbMode
case object ErrorDbMode extends DbMode
case object AppendDbMode extends DbMode

sealed trait GroupNodes
case object NoneGroupNodes extends GroupNodes
case object SubjectGroupNodes extends GroupNodes
case object SubAnonGroupNodes extends GroupNodes

sealed trait LinkNodes
case object NoneLinkNodes extends LinkNodes
case object SameAsLinkNodes extends LinkNodes
case object SameAnonLinkNodes extends LinkNodes

case class Config(props: Map[String,String]) {
  def prop(key: String, pfx: String = "index2neo4j"): String = {
    val p = props.getOrElse(s"$pfx.$key", "") or props.getOrElse(key, "")
    if (p.trim.isEmpty) throw new ConfigParamNotFoundException(s"$pfx.$key")
    p
  }

  def solrurl = prop("solr-url")
  def lucenedir = CacheDir.getCacheFolder("lucene-index")
  def neo4juri = prop("neo4j-uri")
  def neo4datadir = new File(prop("neo4j-datadir"))
  val neo4username = prop("neo4j-username")
  val neo4password = prop("neo4j-password")
  val dbMode: DbMode =
    prop("db-mode").toLowerCase match {
      case "append" => AppendDbMode
      case "error" => ErrorDbMode
      case "clear" => ClearDbMode
      case mode => throw new InvalidConfigParamException("db-mode", mode)
    }
  val neo4jClientMode: Neo4jClientMode =
    prop("neo4j-client-mode").toLowerCase match {
      case "disk" => DiskNeo4jClientMode
      case "disk-fc" => DiskFcNeo4jClientMode
      case "jdbc-fc" => JdbcFcNeo4jClientMode
      case mode => throw new InvalidConfigParamException("neo4j-client-mode", mode)
    }
  val indexClientMode: IndexClientMode =
    prop("index-client-mode").toLowerCase match {
      case "solr" => SolrIndexClientMode
      case "lucene" => LuceneIndexClientMode
      case "memdb" => MemdbIndexClientMode
      case mode => throw new InvalidConfigParamException("index-client-mode", mode)
    }
  val groupNodes: GroupNodes =
    prop("group-nodes").toLowerCase match {
      case "none" => NoneGroupNodes
      case "subject" => SubjectGroupNodes
      case "sub-anon" => SubAnonGroupNodes
      case group => throw new InvalidConfigParamException("group-nodes", group)
    }
  val linkNodes: LinkNodes =
    prop("link-nodes").toLowerCase match {
      case "none" => NoneLinkNodes
      case "sameas" => SameAsLinkNodes
      case "same-anon" => SameAnonLinkNodes
      case link => throw new InvalidConfigParamException("link-nodes", link)
    }
  val nWriters = neo4jClientMode match {
    case DiskNeo4jClientMode => 1
    case DiskFcNeo4jClientMode => 1
    case _ => prop("n-writers").toInt
  }
  val nReaders = indexClientMode match {
    case LuceneIndexClientMode => 1
    case _ => prop("n-readers").toInt
  }
  val limitTriples = try { prop("limit-triples").toLong } catch { case _: Throwable => Long.MaxValue }
  val limitLinks = try { prop("limit-links").toLong } catch { case _: Throwable => Long.MaxValue }
  val linksPerNode = try { prop("links-pernode").toInt } catch { case _: Throwable => Int.MaxValue }
  val linksDrop = try { prop("links-drop").toInt } catch { case _: Throwable => Int.MaxValue }
  val linksAnon = prop("links-anon").isTrue

  var indexClientFactory: IndexClientFactory = _
  var dbClientFactory: DbClientFactory = _
}

class DbClientFactory(config: Config) {
  if (config.neo4jClientMode == JdbcFcNeo4jClientMode)
    Class.forName("org.neo4j.jdbc.Driver")

  val diskDbClient = // single instance
    config.neo4jClientMode match {
      case DiskNeo4jClientMode => Some(new DiskDbClient(config.neo4datadir, config.linksPerNode))
      case DiskFcNeo4jClientMode => Some(new DiskFullCacheDbClient(config.neo4datadir, config.linksPerNode))
      case _ => None
    }

  def executor: JdbcCypherExecutor =
    new JdbcCypherExecutor(config.neo4juri, config.neo4username or null, config.neo4password or null)

  def getClient: DbClient =
    config.neo4jClientMode match {
      case JdbcFcNeo4jClientMode => new JdbcFullCacheDbClient(executor, config.linksPerNode)
      case DiskNeo4jClientMode => diskDbClient.get
      case DiskFcNeo4jClientMode => diskDbClient.get
    }
}

class IndexClientFactory(config: Config) {
  val solrClient = // single instance
    if (config.indexClientMode == SolrIndexClientMode) {
      val httpClient = new HttpSolrClient(config.solrurl)
      Some(new SolrIndexClient(httpClient))
    } else None

  val luceneClient = // single instance
    if (config.indexClientMode == LuceneIndexClientMode) Some(new LuceneIndexClient(config.lucenedir)) else None

  def getClient: IndexClient =
    config.indexClientMode match {
      case SolrIndexClientMode => solrClient.get
      case LuceneIndexClientMode => luceneClient.get
      case MemdbIndexClientMode => new MemdbIndexClient
    }
}

object MasterActor {
  case class StartIN(config: Config)
  case object StopIN
  case class StopOUT(result: Boolean, loaded: Long, total: Long)
}

class MasterActor extends Actor {
  import MasterActor._

  val loader = context.actorOf(Props[IndexToDbLoadActor], name = "index2neo4j.IndexToDbLoadActor")
  val linker = context.actorOf(Props[LinkNodesActor], name = "index2neo4j.LinkNodesActor")

  class State(val config: Config) {
    //println(s"Source Solr ${config.solrurl}")
    //println(s"Target Neo4j ${config.neo4juri}")

    val diskClientMode = config.neo4jClientMode == DiskNeo4jClientMode || config.neo4jClientMode == DiskFcNeo4jClientMode
    if (config.dbMode == ClearDbMode && diskClientMode && config.neo4datadir.exists) {
      println("Deleting Neo4j data directory")
      val marker = new File(config.neo4datadir, "messages.log")
      if (!marker.exists) throw new MarkerFileNotFoundException(marker.getAbsolutePath) // ensure it is Neo4j dir
      if (!new CFile(config.neo4datadir).delete)
        throw new CannotDeletePathException(config.neo4datadir.getAbsolutePath)
    }

    if (config.dbMode == ClearDbMode) {
      println("Deleting DB cache files")
      CacheDir.deleteCacheFolder("neo4j-cache")
    }

    val indexClientFactory = new IndexClientFactory(config)
    config.indexClientFactory = indexClientFactory

    val dbClientFactory = new DbClientFactory(config)
    val dbClient = dbClientFactory.getClient
    config.dbClientFactory = dbClientFactory

    var result: Boolean = false
    var (totalTriples, loadedTriples) = (0L, 0L)
  }

  var state: State = _

  def assertDbMode: Boolean = {
    val n = state.dbClient.countNodes
    println(s"Neo4j contains $n nodes")
    state.config.dbMode match {
      case ClearDbMode if n > 0 =>
        println("Clearing Neo4j")
        state.dbClient.clear
        true
      case ErrorDbMode if n > 0 =>
        println("[ERROR] Neo4j contains data")
        self ! MasterActor.StopIN
        false
      case _ => true
    }
  }

  def closeResources =
    try {
      println("Closing DB client...")
      state.dbClientFactory.getClient.close
    } catch { case t: Throwable => t.printStackTrace }

  override def supervisorStrategy: SupervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 0) {
      case _ =>
        closeResources
        Escalate
    }

  def receive = {
    case StartIN(config) =>
      state = new State(config)
      if (assertDbMode) loader ! IndexToDbLoadActor.StartIN(state.config)

    case IndexToDbLoadActor.StopOUT(total, loaded) =>
      val n = state.dbClient.countNodes
      println(s"Neo4j contains $n nodes")
      if (n > 0) {
        state.totalTriples = total
        state.loadedTriples = loaded
        state.result = true
      }
      state.config.linkNodes match {
        case NoneLinkNodes => self ! StopIN
        case _ => linker ! LinkNodesActor.StartIN(state.config)
      }

    case LinkNodesActor.StopOUT(_, _) =>
      val n = state.dbClient.countNodes
      println(s"Neo4j contains $n nodes")
      self ! StopIN

    case StopIN =>
      closeResources
      context.stop(self)
      context.parent ! StopOUT(state.result, state.loadedTriples, state.totalTriples)
  }

}

object IndexToDbLoadActor {
  case class StartIN(config: Config)
  case class StopOUT(total: Long, loaded: Long)
}

class IndexToDbLoadActor extends Actor with NextShard with ItemsInQueue {
  import IndexToDbLoadActor._

  class State(val config: Config) {
    val indexClient = config.indexClientFactory.getClient
    val total = indexClient.countTriples
    var loaded = 0L
    var nothingRead = false
    val stats = new Stats
    var activeReaders = config.nReaders
  }

  var state: State = _

  class Stats {
    var (startTime, stopTime) = (0L, 0L)
    def start = startTime = System.currentTimeMillis
    def stop = stopTime = System.currentTimeMillis
    def executingTime = Convert.durationWords(stopTime-startTime)

    val df = new DecimalFormat("0.000")
    val (readTimePerTriple, writeTimePerTriple, bufferSize) = (new ListBuffer[Long], new ListBuffer[Long], 1000)
    def tripleRead(time: Long) = if (readTimePerTriple.size < bufferSize) readTimePerTriple += time
    def tripleWrite(time: Long) = if (writeTimePerTriple.size < bufferSize) writeTimePerTriple += time
    val avg = (x: ListBuffer[Long]) => if (x.nonEmpty) df.format(x.sum.toDouble/x.size.toDouble/1000d)+"s" else "?"
    def avgTripleReadTime = avg(readTimePerTriple)
    def avgTripleWriteTime = avg(writeTimePerTriple)
  }

  val progress = context.actorOf(Props[ProgressActor], name = "index2neo4j.ProgressActor")
  var reader: ActorRef = _
  var writer: ActorRef = _

  def createReader =
    reader = context.actorOf(Props(new ReadActor(receiver = self)).
      withRouter(RoundRobinPool(state.config.nReaders)), name = "index2neo4j.ReadActor")

  def createWriter =
    writer = context.actorOf(Props(new WriteActor(state.config)).
      withRouter(RoundRobinPool(state.config.nWriters)), name = "index2neo4j.WriteActor")

  def startReader =
    (1 to state.config.nReaders).foreach { _ =>
      val shard = nextShard(state.total) match {
        case Some(x) => x
        case None => Shard(0, 0)
      }
      reader ! ReadActor.StartIN(shard, state.config)
    }

  def nextShardToReader =
    nextShard(state.total) match {
      case Some(shard) => sender ! ReadActor.StartIN(shard, state.config)
      case None => state.activeReaders -= 1
    }

  override def supervisorStrategy: SupervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 0) { case _ => Escalate }

  def toggleReader =
    if (!state.nothingRead)
      if (hghItemsInQueue) reader ! Broadcast(ReadActor.PauseIN)
      else if (lowItemsInQueue) reader ! Broadcast(ReadActor.ResumeIN)

  def receive = {
    case StartIN(config) =>
      println(s"Loading data from index into Neo4j")
      state = new State(config)
      println(s"Index contains ${state.total} triples")
      progress ! ProgressActor.StartIN(state.total)
      state.stats.start
      createWriter
      createReader
      startReader

    case ReadActor.TripleOUT(triple, time) =>
      incItemsInQueue
      toggleReader
      state.stats.tripleRead(time)
      writer ! WriteActor.TripleIN(triple)

    case ReadActor.SubjectTripleOUT(triples, time) =>
      incItemsInQueue
      toggleReader
      state.stats.tripleRead(time)
      writer ! WriteActor.SubjectTripleIN(triples)

    case ReadActor.NoDataOUT =>
      nextShardToReader
      if (state.activeReaders == 0) {
        state.nothingRead = true
        if (noItemsInQueue) self ! WriteActor.IdleOUT(0, time = 0) // simulate IdleOUT
      }

    case WriteActor.IdleOUT(n, time) =>
      decItemsInQueue
      state.loaded += n
      toggleReader
      if (n > 0) {
        state.stats.tripleWrite(time)
        progress ! ProgressActor.UpdateIN(n)
      }
      if (state.nothingRead && noItemsInQueue) progress ! ProgressActor.StopIN

    case ProgressActor.StopOUT =>
      state.stats.stop
      println("Index commit...")
      state.indexClient.commit
      state.indexClient.optimize
      println(s"Loaded ${state.loaded} triples of ${state.total} in ${state.stats.executingTime}")
      println(s"Avg triple read ${state.stats.avgTripleReadTime} and write ${state.stats.avgTripleWriteTime}")
      context.parent ! StopOUT(state.total, state.loaded)
  }

}

object ReadActor {
  case class StartIN(shard: Shard, config: Config)
  case object LoopIN
  case object PauseIN
  case object ResumeIN
  case class TripleOUT(triple: XTriple, time: Long)
  case class SubjectTripleOUT(triples: Seq[XTriple], time: Long)
  case object NoDataOUT
}

class ReadActor(receiver: ActorRef) extends Actor {
  import ReadActor._

  /** load individual triples */
  class IndiTripleBatch(shard: Shard) {
    var (curIndex, tripleIndex, triplesSize) = (shard.start.toInt, 0, 0)
    var triples: Seq[XTriple] = Nil

    def nextTriple: Option[XTriple] = {
      if (tripleIndex >= triplesSize) { //load the next batch
        val shardDone = curIndex-shard.start >= shard.size
        if (shardDone) triples = Nil
        else {
          triples = state.indexClient.readTriples(curIndex, rows = shard.size.toInt)
          triplesSize = triples.size
          tripleIndex = 0
          curIndex += triplesSize
        }
      }

      if (triples.isEmpty) None
      else {
        tripleIndex += 1
        Some(triples(tripleIndex-1))
      }
    }
  }

  /** load triples grouped by subject */
  class GroupBySubjectBatch(shard: Shard) {
    var (curIndex, subjectIndex, subjectsSize) = (shard.start.toInt, 0, 0)
    var subjects: Seq[String] = Nil

    def nextSubject: Option[String] = {
      if (subjectIndex >= subjectsSize) { //load the next batch
        val shardDone = curIndex-shard.start >= shard.size
        if (shardDone) subjects = Nil
        else {
          subjects = state.indexClient.readDistinctSubjects(curIndex, rows = shard.size.toInt)
          subjectsSize = subjects.size
          subjectIndex = 0
          curIndex += subjectsSize
        }
      }

      if (subjects.isEmpty) None
      else {
        subjectIndex += 1
        Some(subjects(subjectIndex-1))
      }
    }

    def nextTriple: Option[Seq[XTriple]] =
      nextSubject match {
        case Some(aval) => Some(state.indexClient.readTriplesBySubject(aval))
        case _ => None
      }
  }

  class State(val shard: Shard, val config: Config) {
    val indexClient = config.indexClientFactory.getClient
    val indiTripleBatch = new IndiTripleBatch(shard)
    val groupBySubjectBatch = new GroupBySubjectBatch(shard)
    var (count, limit) = (shard.start, config.limitTriples)
  }

  var state: State = _

  var (paused, hasdata) = (false, false)

  def receive = {
    case LoopIN if state.count >= state.limit =>
      paused = true; hasdata = false
      receiver ! NoDataOUT

    case LoopIN if !paused =>
      val start = System.currentTimeMillis
      state.config.groupNodes match {
        case NoneGroupNodes =>
          state.indiTripleBatch.nextTriple match {
            case Some(triple) =>
              state.count += 1
              receiver ! TripleOUT(triple, time = System.currentTimeMillis-start)
              self ! LoopIN
            case _ =>
              paused = true; hasdata = false
              receiver ! NoDataOUT
          }
        case SubjectGroupNodes =>
          state.groupBySubjectBatch.nextTriple match {
            case Some(triples) =>
              state.count += 1
              receiver ! SubjectTripleOUT(triples, time = System.currentTimeMillis-start)
              self ! LoopIN
            case _ =>
              paused = true; hasdata = false
              receiver ! NoDataOUT
          }
        case SubAnonGroupNodes =>
          throw new UnsupportedOperationException("group nodes sub-anon is not supported")
      }

    case StartIN(_, _) if hasdata =>
      throw new IllegalStateException("Start received while more data to process")

    case StartIN(shard, config) =>
      state = new State(shard, config)
      paused = false; hasdata = true
      self ! LoopIN

    case PauseIN if hasdata =>
      paused = true

    case ResumeIN if paused && hasdata =>
      paused = false
      self ! LoopIN

    case Broadcast(x) =>
      self ! x
  }

}

object WriteActor {
  case class TripleIN(triple: XTriple)
  case class SubjectTripleIN(triples: Seq[XTriple])
  case class IdleOUT(n: Int, time: Long)
}

class WriteActor(config: Config) extends Actor {
  import WriteActor._

  val indexClient = config.indexClientFactory.getClient
  val dbClient = config.dbClientFactory.getClient

  def receive = {
    case TripleIN(triple) =>
      val start = System.currentTimeMillis
      dbClient.addTriple(triple)
      indexClient.updateTripleDpk(triple, commit = false)
      sender ! IdleOUT(1, time = System.currentTimeMillis-start)

    case SubjectTripleIN(triples) =>
      val start = System.currentTimeMillis
      dbClient.addSubjectTriples(triples)
      indexClient.updateTriplesDpk(triples, commit = false)
      sender ! IdleOUT(triples.size, time = System.currentTimeMillis-start)
  }

}

object LinkNodesActor {
  case class StartIN(config: Config)
  case class StopOUT(tripleLinks: Long, nodeLinks: Long)
}

class LinkNodesActor extends Actor with NextShard with ItemsInQueue {
  import LinkNodesActor._

  class State(val config: Config) {
    val indexClient = config.indexClientFactory.getClient
    var (totalTriples, tripleLinks, nodeLinks) = (indexClient.countTriples, 0L, 0L)
    var nothingRead = false
    val stats = new Stats
    var activeReaders = config.nReaders
  }

  var state: State = _

  class Stats {
    var (startTime, stopTime) = (0L, 0L)
    def start = startTime = System.currentTimeMillis
    def stop = stopTime = System.currentTimeMillis
    def executingTime = Convert.durationWords(stopTime-startTime)

    val df = new DecimalFormat("0.000")
    val (readTimePerLink, writeTimePerLink, bufferSize) = (new ListBuffer[Long], new ListBuffer[Long], 1000)
    def linkRead(time: Long) = if (readTimePerLink.size < bufferSize) readTimePerLink += time
    def linkWrite(time: Long) = if (writeTimePerLink.size < bufferSize) writeTimePerLink += time
    val avg = (x: ListBuffer[Long]) => if (x.nonEmpty) df.format(x.sum.toDouble/x.size.toDouble/1000d)+"s" else "?"
    def avgLinkReadTime = avg(readTimePerLink)
    def avgLinkWriteTime = avg(writeTimePerLink)
  }

  val progress = context.actorOf(Props[ProgressActor], name = "index2neo4j.LinkNodes.ProgressActor")

  var linkCounter: ActorRef = _
  var reader: ActorRef = _
  var writer: ActorRef = _

  def createLinkCounter=
    linkCounter = context.actorOf(Props(new LinkCountActor(state.config)),
      name = "index2neo4j.LinkCountActor")

  def createReader =
    reader = context.actorOf(Props(new LinkReadActor(receiver = self)).
      withRouter(RoundRobinPool(state.config.nReaders)), name = "index2neo4j.LinkReadActor")

  def createWriter =
    writer = context.actorOf(Props(new LinkWriteActor(state.config)).
      withRouter(RoundRobinPool(state.config.nWriters)), name = "index2neo4j.LinkWriteActor")

  def startReader =
    (1 to state.config.nReaders).foreach { _ =>
      val shard = nextShard(state.totalTriples) match {
        case Some(x) => x
        case None => Shard(0, 0)
      }
      reader ! LinkReadActor.StartIN(shard, state.config)
    }

  def nextShardToReader =
    nextShard(state.totalTriples) match {
      case Some(shard) => sender ! LinkReadActor.StartIN(shard, state.config)
      case None => state.activeReaders -= 1
    }

  override def supervisorStrategy: SupervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 0) { case _ => Escalate }

  def toggleReader =
    if (!state.nothingRead)
      if (hghItemsInQueue) reader ! Broadcast(LinkReadActor.PauseIN)
      else if (lowItemsInQueue) reader ! Broadcast(LinkReadActor.ResumeIN)

  def receive = {
    case StartIN(config) =>
      state = new State(config)
      if (config.limitLinks == 0 || config.limitLinks == Long.MaxValue) {
        println("Counting index triple links")
        createLinkCounter
        linkCounter ! LinkCountActor.CountIN
      } else {
        println("Skipping index triple links count phase")
        self ! LinkCountActor.CountOUT(config.limitLinks)
      }

    case LinkCountActor.CountOUT(n) =>
      println(s"Index contains $n triple links in ${state.totalTriples} triples")
      state.tripleLinks = n
      println(s"Linking nodes in Neo4j")
      progress ! ProgressActor.StartIN(state.tripleLinks)
      state.stats.start
      createWriter
      createReader
      startReader

    case LinkReadActor.TriplesOUT(tripleA, tripleB, time) =>
      incItemsInQueue
      toggleReader
      state.stats.linkRead(time)
      writer ! LinkWriteActor.TriplesIN(tripleA, tripleB)

    case LinkReadActor.NoDataOUT =>
      nextShardToReader
      if (state.activeReaders == 0) {
        state.nothingRead = true
        if (noItemsInQueue) self ! LinkWriteActor.IdleOUT(0, time = 0) // simulate IdleOUT
      }

    case LinkWriteActor.IdleOUT(linked, time) =>
      decItemsInQueue
      if (linked > 0) state.nodeLinks += 1
      toggleReader
      if (linked > 0) state.stats.linkWrite(time)
      progress ! ProgressActor.UpdateIN(1)
      if (state.nothingRead && noItemsInQueue) progress ! ProgressActor.StopIN

    case ProgressActor.StopOUT =>
      state.stats.stop
      println(s"Created ${state.nodeLinks} node links from ${state.tripleLinks} triple links in ${state.stats.executingTime}")
      println(s"Avg link read ${state.stats.avgLinkReadTime} and write ${state.stats.avgLinkWriteTime}")
      context.parent ! StopOUT(state.tripleLinks, state.nodeLinks)
  }

}

object LinkReadActor {
  case class StartIN(shard: Shard, config: Config)
  case object LoopIN
  case object PauseIN
  case object ResumeIN
  case class TriplesOUT(tripleA: XTriple, tripleB: XTriple, time: Long)
  case object NoDataOUT
}

class LinkReadActor(receiver: ActorRef) extends Actor {
  import LinkReadActor._

  /** load all the triples (sorted) */
  class TripleBatch(shard: Shard) {
    var (curIndex, tripleIndex, triplesSize) = (shard.start.toInt, 0, 0)
    var triples: Seq[XTriple] = Nil

    def nextTriple: Option[XTriple] = {
      if (tripleIndex >= triplesSize) { //load the next batch
        val shardDone = curIndex-shard.start >= shard.size
        if (shardDone) triples = Nil
        else {
          triples = state.indexClient.readTriplesWithDatabasePK(curIndex, sorted = true, rows = shard.size.toInt)
          triplesSize = triples.size
          tripleIndex = 0
          curIndex += triplesSize
        }
      }

      if (triples.isEmpty) None
      else {
        tripleIndex += 1
        Some(triples(tripleIndex-1))
      }
    }
  }

  /** load linked triples for each of the triple from TripleBatch */
  class LinkedBatch(shard: Shard) {
    val tripleBatch = new TripleBatch(shard: Shard)
    var currentTriple: Option[XTriple] = None

    // load linked triples
    class LinkedTripleBatch {
      var (curIndex, tripleIndex, triplesSize) = (0, 0, 0)
      var triples: Seq[XTriple] = Nil

      def nextTriple: Option[XTriple] = {
        if (tripleIndex >= triplesSize) { //load the next batch
          triples = state.indexClient.readSubsequentLinkedTriples(curIndex, currentTriple.get, dropLimit = state.config.linksDrop)
          triplesSize = triples.size
          tripleIndex = 0
          curIndex += triplesSize
        }

        if (triples.isEmpty) None
        else {
          tripleIndex += 1
          Some(triples(tripleIndex-1))
        }
      }
    }

    var linkedTripleBatch: LinkedTripleBatch = _

    @tailrec final def nextLinkedTriples: Option[(XTriple,XTriple)] = {
      if (currentTriple.isEmpty) { // start with the next triple
        currentTriple = tripleBatch.nextTriple
        linkedTripleBatch = new LinkedTripleBatch
      }
      if (currentTriple.isEmpty) None // all triples were loaded
      else linkedTripleBatch.nextTriple match {
        case Some(linkedTriple) =>
          Some(currentTriple.get, linkedTriple)
        case None => // try with the next triple
          currentTriple = None
          nextLinkedTriples
      }

    }
  }

  class State(val shard: Shard, val config: Config) {
    val indexClient = config.indexClientFactory.getClient
    val linkedBatch = new LinkedBatch(shard)
    var (count, limit) = (shard.start, config.limitLinks)
  }

  var state: State = _

  var (paused, hasdata) = (false, false)

  def receive = {
    case LoopIN if state.count >= state.limit =>
      paused = true; hasdata = false
      receiver ! NoDataOUT

    case LoopIN if !paused =>
      val start = System.currentTimeMillis
      state.linkedBatch.nextLinkedTriples match {
        case Some((tripleA,tripleB)) =>
          state.count += 1
          receiver ! TriplesOUT(tripleA, tripleB, time = System.currentTimeMillis-start)
          self ! LoopIN
        case _ =>
          paused = true; hasdata = false
          receiver ! NoDataOUT
      }

    case StartIN(_, _) if hasdata =>
      throw new IllegalStateException("Start received while more data to process")

    case StartIN(shard, config) =>
      state = new State(shard, config)
      paused = false; hasdata = true
      self ! LoopIN

    case PauseIN if hasdata =>
      paused = true

    case ResumeIN if paused && hasdata =>
      paused = false
      self ! LoopIN

    case Broadcast(x) =>
      self ! x
  }

}

object LinkWriteActor {
  case class TriplesIN(tripleA: XTriple, tripleB: XTriple)
  case class IdleOUT(linked: Int, time: Long)
}

class LinkWriteActor(config: Config) extends Actor {
  import LinkWriteActor._

  val dbClient = config.dbClientFactory.getClient
  val withAnon = config.linkNodes == SameAnonLinkNodes

  val relationshipNode =
    config.linkNodes match {
      case SameAsLinkNodes => UriNode("http://www.w3.org/2002/07/owl#sameAs")
      case SameAnonLinkNodes => UriNode("http://www.w3.org/2002/07/owl#sameAs")
      case _ => PlainLiteralNode("link", "?")
    }

  def link(tripleA: XTriple, tripleB: XTriple): Int = {
    var linked = 0
    val fn = if (withAnon) (nodes: List[XNode]) => dbClient.linkNodesToAnon(nodes, relationshipNode)
    else (nodes: List[XNode]) => dbClient.linkNodes(nodes, relationshipNode)

    (tripleA.s :: tripleA.o :: Nil).foreach { a =>
      (tripleB.s :: tripleB.o :: Nil).filter(b => a.aval == b.aval && a.dpk != b.dpk) match {
        case xs if xs.nonEmpty => linked += fn(a :: xs)
        case _ =>
      }
    }
    linked
  }

  def receive = {
    case TriplesIN(tripleA, tripleB) =>
      val start = System.currentTimeMillis
      val linked = link(tripleA, tripleB)
      sender ! IdleOUT(linked, time = System.currentTimeMillis-start)
  }

}

object LinkCountActor {
  case object CountIN
  case class CountOUT(n: Long)
}

class LinkCountActor(config: Config) extends Actor with NextShard {
  import LinkCountActor._

  val progress = context.actorOf(Props[ProgressActor], name = "index2neo4j.LinkCount.ProgressActor")
  var reader: ActorRef = _

  def createReader =
    reader = context.actorOf(Props(new LinkCountReadActor(receiver = self)).
      withRouter(RoundRobinPool(config.nReaders)), name = "index2neo4j.LinkCountReadActor")

  def startReader =
    (1 to config.nReaders).foreach { _ =>
      val shard = nextShard(state.total) match {
        case Some(x) => x
        case None => Shard(0, 0)
      }
      reader ! LinkCountReadActor.CountIN(shard, config)
    }

  def nextShardToReader =
    nextShard(state.total) match {
      case Some(shard) => sender ! LinkCountReadActor.CountIN(shard, config)
      case None => state.activeReaders -= 1
    }

  class State {
    val indexClient = config.indexClientFactory.getClient
    val total = indexClient.countTriples
    var tripleLinks = 0L
    var activeReaders = config.nReaders
  }

  var state: State = _

  override def supervisorStrategy: SupervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 0) { case _ => Escalate }

  def receive = {
    case CountIN =>
      state = new State
      progress ! ProgressActor.StartIN(state.total)
      createReader
      startReader

    case LinkCountReadActor.CountOUT(n) =>
      state.tripleLinks += n
      nextShardToReader
      if (state.activeReaders == 0) progress ! ProgressActor.StopIN

    case LinkCountReadActor.UpdateOUT(n) =>
      progress ! ProgressActor.UpdateIN(n)


    case ProgressActor.StopOUT =>
      context.parent ! CountOUT(state.tripleLinks)
  }
}

object LinkCountReadActor {
  case class CountIN(shard: Shard, config: Config)
  case class CountOUT(n: Long)
  case class UpdateOUT(n: Long)
}

class LinkCountReadActor(receiver: ActorRef) extends Actor {
  import LinkCountReadActor._

  /** load all the triples (sorted) */
  class TripleBatch(shard: Shard) {
    var (curIndex, tripleIndex, triplesSize) = (shard.start.toInt, 0, 0)
    var triples: Seq[XTriple] = Nil

    def nextTriple: Option[XTriple] = {
      if (tripleIndex >= triplesSize) { //load the next batch
        val shardDone = curIndex-shard.start >= shard.size
        if (shardDone) triples = Nil
        else {
          triples = state.indexClient.readTriplesWithDatabasePK(curIndex, sorted = true, rows = shard.size.toInt)
          triplesSize = triples.size
          tripleIndex = 0
          curIndex += triplesSize
        }
      }

      if (triples.isEmpty) None
      else {
        tripleIndex += 1
        Some(triples(tripleIndex-1))
      }
    }
  }

  /** count linked triples for each of the triple from TripleBatch */
  class CountLinkedBatch(shard: Shard) {
    val tripleBatch = new TripleBatch(shard)
    var currentTriple: Option[XTriple] = None
    var total = 0L

    @tailrec final def countLinkedTriples: Long = {
      if (currentTriple.isEmpty) { // start with the next triple
        currentTriple = tripleBatch.nextTriple
      }
      if (currentTriple.isEmpty) total // all triples were counted
      else {
        total += state.indexClient.countLinkedTriples(currentTriple.get, dropLimit = state.config.linksDrop)
        receiver ! UpdateOUT(1)
        // try with the next triple
        currentTriple = None
        countLinkedTriples
      }
    }
  }

  class State(val shard: Shard, val config: Config) {
    val indexClient = config.indexClientFactory.getClient
    val countLinkedBatch = new CountLinkedBatch(shard)
  }

  var state: State = _

  def receive = {
    case CountIN(shard, config) =>
      state = new State(shard, config)
      val count = state.countLinkedBatch.countLinkedTriples // blocks
      //println(shard+" COUNT "+count)
      receiver ! CountOUT(count)
  }

}
