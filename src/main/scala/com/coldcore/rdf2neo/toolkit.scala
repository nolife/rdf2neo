package com.coldcore.rdf2neo
package toolkit

import org.apache.commons.lang3.time.DurationFormatUtils
import org.apache.log4j.{Logger, Level, PatternLayout, ConsoleAppender}
import java.util.{UUID, Properties}
import scala.collection.JavaConverters._
import java.io.File
import org.mapdb.DBMaker
import com.coldcore.misc5.CFile

object Log {
  def init {
    val console = new ConsoleAppender
    val PATTERN = "%p [%C{1}] %m%n"
    console.setLayout(new PatternLayout(PATTERN))
    console.setThreshold(Level.INFO)
    console.activateOptions
    Logger.getRootLogger.addAppender(console)
    Logger.getRootLogger.setLevel(Level.WARN)
  }
}

object ConfigLoader {
  def load(): Map[String,String] = load("config.properties")
  def load(filename: String): Map[String,String] = {
    val prop = new Properties
    try {
      withOpen(ConfigLoader.getClass.getResourceAsStream("/"+filename)) { in => prop.load(in) }
    } catch { case e: NullPointerException => throw new CannotReadConfigException(filename) }
    prop.asScala.toMap
  }
}

object XNode {
  val TYPE_UriNode = 1
  val TYPE_PlainLiteralNode = 2
  val TYPE_TypeLiteralNode = 3
  val TYPE_AnonNode = 4
}

sealed trait XNode {
  val aval: String // absolute node value
  var uuid: String = UUID.randomUUID.toString // node unique ID
  var dpk = 0L // database node primary key (subject and object nodes)
  val `type`: Int // node type
}
case class TypeLiteralNode(value: String, datatypeUri: String) extends XNode {
  override val aval = s""""$value"^^$datatypeUri"""  // "-0384"^^type
  override val `type` = XNode.TYPE_TypeLiteralNode
}
case class PlainLiteralNode(value: String, language: String) extends XNode {
  override val aval = s""""$value"@$language""" // "Aristotle"@en
  override val `type` = XNode.TYPE_PlainLiteralNode
}
case class UriNode(uri: String) extends XNode {
  override val aval = uri
  override val `type` = XNode.TYPE_UriNode
  def value = uri.split("/").last
}
case class AnonNode(idval: String) extends XNode {
  override val aval = idval
  override val `type` = XNode.TYPE_AnonNode
}

/** Triple contains data for Subject / Predicate / Object */
case class XTriple(s: XNode, p: XNode, o: XNode) {
  var id: Long = _ // triple index (Solr) ID
}

object Convert {
  /** Convert mills into sec.
    *  1024 -> 1.024   0 -> 0.000   10 -> 0.010
    */
  def millsToSec(time: Long): String = {
    val s = "".padTo(4-(""+time).length, "0").mkString+time
    s.substring(0, s.length-3)+"."+s.substring(s.length-3)
  }

  def durationWords(time: Long, countdown: Boolean = false): String =
  if (time < 0) "0 seconds"
  else {
    val s = DurationFormatUtils.formatDurationWords(time, true, true)
    val s0 = if (countdown && s.contains(" second")) s.split(" ").dropRight(2).mkString(" ") else s
    if (s0.nonEmpty) s0 else s
  }
}

case class Shard(start: Long, size: Long)

trait NextShard {
  var shardCount = 0
  val shardSize = 100

  def nextShard(total: Long): Option[Shard] = {
    if (shardCount*shardSize >= total) None
    else {
      val shard = Shard(shardCount*shardSize, shardSize)
      shardCount += 1
      Some(shard)
    }
  }
}

trait ItemsInQueue {
  var itemsInQueue = 0
  val (itemsInQueueMin, itemsInQueueMax) = (10, 100)

  def incItemsInQueue = itemsInQueue += 1
  def decItemsInQueue = itemsInQueue -= 1
  def lowItemsInQueue = itemsInQueue < itemsInQueueMin
  def hghItemsInQueue = itemsInQueue > itemsInQueueMax
  def noItemsInQueue = itemsInQueue <= 0
}

object DbMap {
  private var cacheIndex = System.currentTimeMillis

  def createMap[K,V](folder: String): JMap[K,V] = createMap[K,V](folder, "")

  def createMap[K,V](folder: String, cacheId: String): JMap[K,V] = {
    cacheIndex += 1
    if (cacheId.nonEmpty)
      DBMaker.newFileDB(new File(CacheDir.getCacheFolder(folder), cacheId))
        .closeOnJvmShutdown
        .transactionDisable
        .make
        .getHashMap("temp")
    else
      DBMaker.newFileDB(new File(CacheDir.getCacheFolder(folder), cacheIndex.toString))
        .deleteFilesAfterClose
        .closeOnJvmShutdown
        .transactionDisable
        .make
        .createHashMap("temp")
        .make[K,V]
  }
}

object CacheDir {
  private var cachedir: File = _

  def getCacheFolder(folder: String) = {
    val dir = new File(cachedir, folder)
    if (!dir.exists) dir.mkdirs
    dir
  }

  def deleteCacheFolder(folder: String) = {
    val dir = getCacheFolder(folder)
    if (dir.exists && !new CFile(dir).delete)
      throw new CannotDeletePathException(dir.getAbsolutePath)
  }

  def initCacheDir(cachedir: File) {
    val marker = new File(cachedir, "cachedir")
    if (!cachedir.exists) {
      cachedir.mkdirs
      new CFile(marker).truncateFile(0)
    }
    if (!marker.exists) throw new MarkerFileNotFoundException(marker.getAbsolutePath)
    this.cachedir = cachedir
  }
}