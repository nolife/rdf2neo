package com.coldcore.rdf2neo

import com.coldcore.rdf2neo.toolkit._
import akka.actor.SupervisorStrategy.Resume
import akka.actor._
import java.io.File
import akka.actor.DeadLetter
import akka.actor.OneForOneStrategy

object Runner extends App {
  Log.init

  val config = Config(ConfigLoader.load)
  val system = ActorSystem("RDF2NEO")

  CacheDir.initCacheDir(config.cachedir)

  val listener = system.actorOf(Props(classOf[Listener]))
  system.eventStream.subscribe(listener, classOf[DeadLetter])

  val runner = system.actorOf(Props[RunnerActor], name = "RunnerActor")
  runner ! RunnerActor.StartIN(config)
}

class Listener extends Actor {
  def receive = {
    case x @ DeadLetter(_, _, _) =>
      println(s"Dead letter: $x")
  }
}

case class Config(props: Map[String,String]) {
  def prop(key: String, pfx: String = ""): String = {
    val p = props.getOrElse(s"$pfx.$key", "") or props.getOrElse(key, "")
    if (p.trim.isEmpty) throw new ConfigParamNotFoundException(s"$pfx.$key")
    p
  }

  val cachedir = new File(prop("cache-dir"))

  val file2tdbEnable = prop("enable", "file2tdb").isTrue
  val tdb2indexEnable = prop("enable", "tdb2index").isTrue
  val index2neo4jEnable = prop("enable", "index2neo4j").isTrue
}

object RunnerActor {
  case class StartIN(config: Config)
  case object NextPhaseIN
  case object ShutdownIN
}

class RunnerActor extends Actor {
  import RunnerActor._

  override def supervisorStrategy: SupervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 0) {
      case e: Throwable =>
        println("[ERROR] "+e.getMessage)
        e.printStackTrace
        self ! ShutdownIN
        Resume
    }

  val PHASE_file2tdb = 1
  val PHASE_tdb2index = 2
  val PHASE_index2neo4j = 3

  class State(val config: Config) {
    var phaseIndex = 0
    val startTime = System.currentTimeMillis
  }
  var state: State = _

  def receive = {
    case StartIN(config) =>
      state = new State(config)
      self ! NextPhaseIN

    case NextPhaseIN =>
      state.phaseIndex += 1
      state.phaseIndex match {
        case PHASE_file2tdb =>
          if (state.config.file2tdbEnable) {
            println("Starting file2tdb.MasterActor")
            val file2tdbMaster = context.actorOf(Props[file2tdb.MasterActor], name = "file2tdb.MasterActor")
            file2tdbMaster ! file2tdb.MasterActor.StartIN(file2tdb.Config(state.config.props))
          } else {
            self ! NextPhaseIN
          }
        case PHASE_tdb2index =>
          if (state.config.tdb2indexEnable) {
            println("Starting tdb2index.MasterActor")
            val tdb2indexMaster = context.actorOf(Props[tdb2index.MasterActor], name = "tdb2index.MasterActor")
            tdb2indexMaster ! tdb2index.MasterActor.StartIN(tdb2index.Config(state.config.props))
          } else {
            self ! NextPhaseIN
          }
        case PHASE_index2neo4j =>
          if (state.config.index2neo4jEnable) {
            println("Starting index2neo4j.MasterActor")
            val index2neo4jMaster = context.actorOf(Props[index2neo4j.MasterActor], name = "index2neo4j.MasterActor")
            index2neo4jMaster ! index2neo4j.MasterActor.StartIN(index2neo4j.Config(state.config.props))
          } else {
            self ! NextPhaseIN
          }
        case _ =>
          self ! ShutdownIN
      }

    case file2tdb.MasterActor.StopOUT(result, loaded) =>
      if (!result) {
        println("file2tdb.MasterActor FAILED")
        self ! ShutdownIN
      } else {
        println(s"file2tdb.MasterActor loaded $loaded triples")
        self ! NextPhaseIN
      }

    case tdb2index.MasterActor.StopOUT(result, loaded, total) =>
      if (!result) {
        println("tdb2index.MasterActor FAILED")
        self ! ShutdownIN
      } else {
        println(s"tdb2index.MasterActor loaded $loaded triples (skipped ${total-loaded})")
        self ! NextPhaseIN
      }

    case index2neo4j.MasterActor.StopOUT(result, loaded, total) =>
      if (!result) {
        println("index2neo4j.MasterActor FAILED")
        self ! ShutdownIN
      } else {
        println(s"index2neo4j.MasterActor loaded $loaded triples (skipped ${total-loaded})")
        self ! NextPhaseIN
      }

    case ShutdownIN =>
      val stopTime = System.currentTimeMillis
      println("Total execution time "+Convert.durationWords(stopTime-state.startTime))
      println("Shutting down")
      context.stop(self)
      context.system.shutdown
  }
}

