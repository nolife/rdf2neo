package com.coldcore.rdf2neo
package toolkit
package tdb

import java.io.File
import com.hp.hpl.jena.query._
import com.hp.hpl.jena.tdb.{TDBLoader, TDBFactory}
import com.hp.hpl.jena.rdf.model.{RDFNode, Model}

object TdbToolkit {
  def openDataset(tdbdir: File): Dataset = TDBFactory.createDataset(tdbdir.getAbsolutePath)
  def closeDataset(dataset: Dataset) = dataset.close
  def dsmodel(dataset: Dataset, modelName: String): Model =
    if (modelName.isEmpty) dataset.getDefaultModel else dataset.getNamedModel(modelName)
  def loadModel(dataset: Dataset, modelName: String, url: String) =
    TDBLoader.loadModel(dsmodel(dataset, modelName), url)
  def printModelName(modelName: String) = if (modelName.isEmpty) "Default" else modelName
  def isDefaultModel(modelName: String) = modelName.isEmpty

  def countTriples(dataset: Dataset, modelName: String): Long = {
    val strQuery= "SELECT (count(*) AS ?count) { ?s ?p ?o }"
    val query = QueryFactory.create(strQuery)
    val qexe = QueryExecutionFactory.create(query, dsmodel(dataset, modelName))
    val rs = qexe.execSelect
    rs.next().getLiteral("count").getLong
  }

  def printTriples(dataset: Dataset, limit: Int, modelName: String) {
    val strQuery= s"SELECT ?s ?p ?o { ?s ?p ?o } LIMIT $limit"
    val query = QueryFactory.create(strQuery)
    val qexe = QueryExecutionFactory.create(query, dsmodel(dataset, modelName))
    val rs = qexe.execSelect
    ResultSetFormatter.out(System.out, rs)
  }

  def readTriples(dataset: Dataset, modelName: String): ResultSet = {
    val strQuery= s"SELECT ?s ?p ?o { ?s ?p ?o }"
    val query = QueryFactory.create(strQuery)
    val qexe = QueryExecutionFactory.create(query, dsmodel(dataset, modelName))
    val rs = qexe.execSelect
    rs
  }

  def makeTriple(x: QuerySolution): Option[XTriple] = {
    def node = (n: RDFNode) =>
      if (n.isLiteral) {
        val x = n.asLiteral
        Some(Option(x.getDatatypeURI) match {
          case Some(_) => TypeLiteralNode(x.getString, x.getDatatypeURI)
          case None => PlainLiteralNode(x.getString, x.getLanguage)
        })
      }
      else if (n.isURIResource) Some(UriNode(n.asResource.getURI))
      else if (n.isAnon) Some(AnonNode(n.asResource.getId.toString))
      else None //do not know how to convert

    (node(x.get("s")), node(x.get("p")), node(x.get("o"))) match {
      case (s, p, o) if s.isDefined && p.isDefined && o.isDefined => Some(XTriple(s.get, p.get, o.get))
      case _ => None
    }
  }

}
