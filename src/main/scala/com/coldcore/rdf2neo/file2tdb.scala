package com.coldcore.rdf2neo
package file2tdb

import akka.actor._
import java.io.File
import java.text.DecimalFormat
import com.coldcore.rdf2neo.toolkit.Convert
import com.hp.hpl.jena.query.Dataset
import akka.actor.SupervisorStrategy.Escalate
import com.coldcore.rdf2neo.toolkit.tdb.TdbToolkit
import java.nio.file.Paths
import com.coldcore.misc5.CFile

sealed trait ModelMode
case object DeleteModelMode extends ModelMode
case object ErrorModelMode extends ModelMode
case object AppendModelMode extends ModelMode

case class Config(props: Map[String,String]) {
  def prop(key: String, pfx: String = "file2tdb"): String = {
    val p = props.getOrElse(s"$pfx.$key", "") or props.getOrElse(key, "")
    if (p.trim.isEmpty) throw new ConfigParamNotFoundException(s"$pfx.$key")
    p
  }

  val file = new File(prop("file"))
  val tdbdir = new File(prop("tdb-dir"))
  val modelName =
    prop("tdb-model-name").toLowerCase match {
      case "default" => ""
      case mname => mname
    }
  val modelMode: ModelMode =
    prop("tdb-model-mode").toLowerCase match {
      case "append" => AppendModelMode
      case "error" => ErrorModelMode
      case "delete" => DeleteModelMode
      case mode => throw new InvalidConfigParamException("tdb-model-mode", mode)
    }
  val defaultModelName = modelName.isEmpty ? true | false
  val modelNameStr = defaultModelName ? "Default" | modelName

  var dataset: Dataset = _
}

object MasterActor {
  case class StartIN(config: Config)
  case object StopIN
  case class StopOUT(result: Boolean, loaded: Long)
}

class MasterActor extends Actor {
  import MasterActor._

  val loader = context.actorOf(Props[FileToTdbLoadActor], name = "file2tdb.FileToTdbLoadActor")

  class State(val config: Config) {
    //println(s"Source file ${config.file.getAbsolutePath}")
    //println(s"Target TDB ${config.tdbdir.getAbsolutePath}")
    //println(s"TDB model ${config.modelNameStr}")

    if (config.modelMode == DeleteModelMode && config.tdbdir.exists) {
      println("Deleting TDB directory")
      val marker = new File(config.tdbdir, "journal.jrnl")
      if (!marker.exists) throw new MarkerFileNotFoundException(marker.getAbsolutePath) // ensure it is TDB dir
      if (!new CFile(config.tdbdir).delete)
        throw new CannotDeletePathException(config.tdbdir.getAbsolutePath)
    }

    val dataset = TdbToolkit.openDataset(config.tdbdir)
    config.dataset = dataset

    var result: Boolean = false
    var loadedTriples = 0L
  }

  var state: State = _

  def assertModelMode: Boolean = {
    val n = TdbToolkit.countTriples(state.dataset, state.config.modelName)
    println(s"TDB contains $n triples")
    state.config.modelMode match {
      case ErrorModelMode if n > 0 =>
        println("[ERROR] TDB contains data, model " + state.config.modelNameStr)
        self ! MasterActor.StopIN
        false
      case _ => true
    }
  }

  override def supervisorStrategy: SupervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 0) {
      case _ =>
        closeResources
        Escalate
    }

  def closeResources =
    try {
      println("Closing TDB dataset...")
      TdbToolkit.closeDataset(state.dataset)
    } catch { case t: Throwable => t.printStackTrace }

  def receive = {
    case StartIN(config) =>
      state = new State(config)
      if (assertModelMode) loader ! FileToTdbLoadActor.StartIN(state.config)

    case FileToTdbLoadActor.StopOUT =>
      val n = TdbToolkit.countTriples(state.dataset, state.config.modelName)
      println(s"TDB contains $n triples")
      if (n > 0) {
        state.result = true
        state.loadedTriples = n
      }
      self ! StopIN

    case StopIN =>
      closeResources
      context.stop(self)
      context.parent ! StopOUT(state.result, state.loadedTriples)
  }

}

object FileToTdbLoadActor {
  case class StartIN(config: Config)
  case object StopOUT
}

class FileToTdbLoadActor extends Actor {
  import FileToTdbLoadActor._

  case class State(config: Config) {
    val dataset = config.dataset
    val stats = new Stats
  }

  class Stats {
    var (startTime, stopTime) = (0L, 0L)
    def start = startTime = System.currentTimeMillis
    def stop = stopTime = System.currentTimeMillis
    def executingTime = Convert.durationWords(stopTime-startTime)
  }

  var state: State = _
  val progress = context.actorOf(Props[ProgressActor], name = "file2tdb.ProgressActor")

  override def supervisorStrategy: SupervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 0) { case _ => Escalate }

  def load {
    val path = Paths.get(state.config.file.getAbsolutePath)
    TdbToolkit.loadModel(state.dataset, state.config.modelName, path.toUri.toString) //blocks
  }

  def receive = {
    case StartIN(config) =>
      println("Loading file into TDB")
      if (!config.defaultModelName) println(s"Loading into named model will result in slow progress")
      state = State(config)
      progress ! ProgressActor.StartIN(config)
      state.stats.start
      load //blocks
      state.stats.stop
      progress ! ProgressActor.StopIN

    case ProgressActor.StopOUT =>
      println(s"Loaded triples from file in ${state.stats.executingTime}")
      context.parent ! StopOUT
  }
}

object ProgressActor {
  case class StartIN(config: Config)
  case object StopIN
  case object StopOUT
  case object LoopIN
}

class ProgressActor extends Actor {
  import ProgressActor._

  case class State(config: Config) {
    val tdbDirWrapper = new CFile(config.tdbdir)
    val tdbDirSize = () => if (tdbDirWrapper.getFile.exists) tdbDirWrapper.size(0) else 0
    val tdbDirSize0 = tdbDirSize()

    val readFileWrapper = config.file
    val readFileSize = () => if (readFileWrapper.exists) readFileWrapper.length else 0
    val readFileSize0 = readFileSize()

    val df = new DecimalFormat("0.00000")
    val start = System.currentTimeMillis
    val sleep = 1000L // update every secons
    var looping = false
    var lastPrintedLine = ""
    var (spinner, spos) = ("-\\|/-\\|/", 0) // spinner and its sequence position
  }

  var state: State = _

  val complete = () => {
    val d = state.tdbDirSize()-state.tdbDirSize0
    val x = d.toDouble*100d/state.readFileSize0.toDouble
    if (state.config.defaultModelName) x else x/2.4d // when using non-default model it is much slower 100% = 240%
  }

  def loop {
    val prc = complete()
    val line = s"\r --<[ ${state.df.format(prc)}% ]>-- --<[ ${state.spinner(state.spos)} ]>-- "
    val pads = (0 to state.lastPrintedLine.size-line.size).map(_ => " ").mkString
    print(s"\r $line$pads ")
    state.lastPrintedLine = line
    state.spos += 1; if (state.spos >= state.spinner.size) state.spos = 0
    Thread.sleep(state.sleep)
  }

  def receive = {
    case StartIN(config) =>
      state = State(config)
      state.looping = true
      self ! LoopIN

    case LoopIN if state.looping =>
      loop
      self ! LoopIN

    case StopIN =>
      state.looping = false
      print("\r "+state.lastPrintedLine.map(_ => " ").mkString)
      println("\r --<[ 100% ]>-- --<[ "+Convert.durationWords(System.currentTimeMillis-state.start)+" ]>-- ")
      sender ! StopOUT
  }
}
