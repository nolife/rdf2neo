package com.coldcore.rdf2neo
package actors

import akka.actor.Actor
import java.text.DecimalFormat
import com.coldcore.rdf2neo.toolkit.Convert
import org.apache.commons.math3.stat.regression.SimpleRegression

object ProgressActor {
  case class StartIN(total: Long)
  case class UpdateIN(n: Long)
  case object StopIN
  case object StopOUT
}

class ProgressActor extends Actor {
  import ProgressActor._

  class State(val total: Long) {
    val df = new DecimalFormat("0.00000")
    val start = System.currentTimeMillis
    var (current, lastPrintedLine) = (0L, "")
    var (lastRtimeLine, lastRtimeUpdate) = ("", start) // remaining time (simple)
    val (rtimeAt, rtimeEvery) = (100, 1000)            // ... start at current=100 and update every second
    val totalmem = Runtime.getRuntime.maxMemory
    val regressor = new SimpleRegression // remaining time (linear regression)
    var regressorItems = 0L              // ...
    var (lastFmemLine, lastFmemUpdate) = ("", start) // free memory
    val fmemEvery = 1000                             // ... and update every second
  }

  var state: State = _

  /** Linear regression estimation */
  def remainingTime: String = {
    val cur = System.currentTimeMillis
    if (cur-state.lastRtimeUpdate >= state.rtimeEvery) {
      val (diff, ritems, regressor) = (cur-state.start, state.total-state.current, state.regressor)
      regressor.addData(ritems, diff)
      state.regressorItems += 1
      if (state.regressorItems >= 3) {
        val ms = regressor.getIntercept.toLong-diff
        state.lastRtimeLine = " --<[ " + Convert.durationWords(ms, countdown = true) + " ]>-- "
      }
      state.lastRtimeUpdate = cur
    }
    state.lastRtimeLine
  }

/* Simple estimation
  def remainingTime: String = {
    val (cur, rtimeAt, rtimeEvery) = (System.currentTimeMillis, state.rtimeAt, state.rtimeEvery)
    if (state.current >= rtimeAt && cur-state.lastRtimeUpdate >= rtimeEvery) {
      val (diff, current, ritems) = (cur-state.start, state.current, state.total-state.current)
      val ms = (ritems.toDouble/current.toDouble*diff.toDouble).toLong
      state.lastRtimeLine = " --<[ " + Convert.durationWords(ms, countdown = true) + " ]>-- "
      state.lastRtimeUpdate = cur
    }
    state.lastRtimeLine
  }
*/

  def remainingCount: String =
    s" --<[ ${state.current}/${state.total} ]>-- "

  def freeMemory: String = {
    val cur = System.currentTimeMillis
    if (cur-state.lastFmemUpdate >= state.fmemEvery) {
      val freemem = Runtime.getRuntime.freeMemory
      val usedmem = state.totalmem-freemem
      state.lastFmemLine = s" --<[ ${usedmem/1024/1024}/${state.totalmem/1024/1024} MB ]>-- "
      state.lastFmemUpdate = cur
    }
    state.lastFmemLine
  }

  def update(n: Long) {
    state.current += n
    val prc = state.current.toDouble*100d/state.total.toDouble
    val line = s"\r --<[ ${state.df.format(prc)}% ]>-- "+remainingCount+remainingTime+freeMemory
    val pads = (0 to state.lastPrintedLine.size-line.size).map(_ => " ").mkString
    print(s"\r $line$pads ")
    state.lastPrintedLine = line
  }

  def receive = {
    case StartIN(total: Long) =>
      state = new State(total)

    case UpdateIN(n: Long) =>
      update(n)

    case StopIN =>
      //println // uncomment this to see the actual stop value
      print("\r "+state.lastPrintedLine.map(_ => " ").mkString)
      println("\r --<[ 100% ]>-- --<[ "+Convert.durationWords(System.currentTimeMillis-state.start)+" ]>-- ")
      sender ! StopOUT
  }

}
