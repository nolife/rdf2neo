package com.coldcore.rdf2neo
package toolkit
package index

import java.io.File

import org.apache.http.MethodNotSupportedException
import org.apache.lucene.analysis.standard.StandardAnalyzer
import org.apache.lucene.document._
import org.apache.lucene.facet.index.CategoryDocumentBuilder
import org.apache.lucene.facet.search.FacetsCollector
import org.apache.lucene.facet.search.params.{CountFacetRequest, FacetSearchParams}
import org.apache.lucene.facet.taxonomy.directory.{DirectoryTaxonomyReader, DirectoryTaxonomyWriter}
import org.apache.lucene.facet.taxonomy.CategoryPath
import org.apache.lucene.index._
import org.apache.lucene.search._
import org.apache.lucene.store.FSDirectory
import org.apache.lucene.util.Version
import org.apache.solr.client.solrj.SolrQuery
import org.apache.solr.client.solrj.impl.HttpSolrClient
import org.apache.solr.common.params.FacetParams
import org.apache.solr.common.{SolrDocument, SolrInputDocument}
import scala.collection.JavaConverters._
import scala.collection.mutable
import com.google.common.cache.CacheBuilder

object IndexToolkit {
  /** Escape special characters.
    *  + - && || ! ( ) { } [ ] ^ \" ~ * ? : \\
    */
  def escapeChars(x: String): String = {
    val r = List("\\", "+", "-", "&&", "||", "!", "(", ")", "{", "}", "[", "]", "^", "\"", "~", "*", "?", ":")
    (x /: r)((a, b) => a.replace(b, "\\"+b))
  }

  def serializeTriple(triple: XTriple): SolrInputDocument = {
    val doc = new SolrInputDocument
    doc.addField("id", triple.id)
    doc.addField("s_typ", triple.s.`type`)
    doc.addField("p_typ", triple.p.`type`)
    doc.addField("o_typ", triple.o.`type`)
    doc.addField("s_avl", triple.s.aval)
    doc.addField("p_avl", triple.p.aval)
    doc.addField("o_avl", triple.o.aval)

    doc.addField("s_dpk", triple.s.dpk)
    doc.addField("o_dpk", triple.o.dpk)

    //absolute value breakdown into dynamic fields
    val dyn = (node: XNode, x: String) => {
      doc.addField(s"${x}_uid", node.uuid)
      node match {
        case TypeLiteralNode(value, datatypeUri) =>
          doc.addField(s"${x}_dn_val", value)
          doc.addField(s"${x}_dn_dtu", datatypeUri)
        case PlainLiteralNode(value, language) =>
          doc.addField(s"${x}_dn_val", value)
          doc.addField(s"${x}_dn_lng", language)
        case un @ UriNode(uri) =>
          doc.addField(s"${x}_dn_uri", uri)
          doc.addField(s"${x}_dn_val", un.value)
        case AnonNode(idval) =>
          doc.addField(s"${x}_dn_aid", idval)
      }
    }
    dyn(triple.s, "s")
    dyn(triple.p, "p")
    dyn(triple.o, "o")

    doc
  }

  def deserializeTriple(doc: SolrDocument): XTriple = {
    val id = doc.getFieldValue("id").toString.toLong
    val node = (x: String) => {
      val typ = doc.getFieldValue(s"${x}_typ").toString.toInt
      val aval = doc.getFieldValue(s"${x}_avl").toString
      val n = typ match {
        case XNode.TYPE_TypeLiteralNode =>
          val value = doc.getFieldValue(s"${x}_dn_val").toString
          val datatypeUri = doc.getFieldValue(s"${x}_dn_dtu").toString
          TypeLiteralNode(value, datatypeUri)
        case XNode.TYPE_PlainLiteralNode =>
          val value = doc.getFieldValue(s"${x}_dn_val").toString
          val language = doc.getFieldValue(s"${x}_dn_lng").toString
          PlainLiteralNode(value, language)
        case XNode.TYPE_UriNode=>
          val uri = doc.getFieldValue(s"${x}_dn_uri").toString
          UriNode(uri)
        case XNode.TYPE_AnonNode=>
          val idval = doc.getFieldValue(s"${x}_dn_aid").toString
          AnonNode(idval)
        case _ => throw new IllegalStateException(s"Unknown type $typ on triple #$id")
      }
      n.uuid = doc.getFieldValue(s"${x}_uid").toString
      if ("so".contains(x)) n.dpk = doc.getFieldValue(s"${x}_dpk").toString.toLong
      assert(n.aval == aval, s"Triple #$id absolute value mismatch, ${n.aval} must be equal to $aval")
      n
    }

    XTriple(node("s"), node("p"), node("o")) |< { x => x.id = id }
  }
}

sealed trait IndexClientMode
case object SolrIndexClientMode extends IndexClientMode
case object LuceneIndexClientMode extends IndexClientMode
case object MemdbIndexClientMode extends IndexClientMode

trait IndexClient {
  def clear
  def countRecords: Long
  def countTriples: Long
  def lookupTripleId(triple: XTriple): Option[Long]
  def addTriple(triple: XTriple, commit: Boolean = true)
  def addTriples(triples: Seq[XTriple], commit: Boolean = true)
  def updateTripleDpk(triple: XTriple, commit: Boolean = true)
  def updateTriplesDpk(triples: Seq[XTriple], commit: Boolean = true)
  def readTriples(start: Int, sorted: Boolean = false, rows: Int = 100): Seq[XTriple]
  def readTriplesWithDatabasePK(start: Int, sorted: Boolean = false, rows: Int = 100): Seq[XTriple]
  def readSubsequentLinkedTriples(start: Int, triple: XTriple, rows: Int = 100, dropLimit: Int = Int.MaxValue): Seq[XTriple]
  def countLinkedTriples(triple: XTriple, dropLimit: Int = Int.MaxValue): Long
  def readDistinctSubjects(start: Int, rows: Int = 100): Seq[String]
  def readTriplesBySubject(aval: String, rows: Int = 100): Seq[XTriple]
  def commit
  def optimize
}

class SolrIndexClient(client: HttpSolrClient) extends IndexClient {
  import IndexToolkit._

  val allTriplesQuery = "s_avl:*"

  override def clear {
    client.deleteByQuery("*:*")
    client.commit
    client.optimize
    assert(countRecords == 0)
  }

  def count(query: String): Long = {
    val params = new SolrQuery
    params.set("q", query)
    params.setRows(0)
    val response = client.query(params)
    response.getResults.getNumFound
  }

  override def countRecords: Long =
    count("*:*")

  override def countTriples: Long =
    count(allTriplesQuery)

  override def lookupTripleId(triple: XTriple): Option[Long] = {
    val params = new SolrQuery
    val query = s"""s_avl:"${escapeChars(triple.s.aval)}" AND p_avl:"${escapeChars(triple.p.aval)}" AND o_avl:"${escapeChars(triple.o.aval)}""""
    params.set("q", query)
    val response = client.query(params)
    response.getResults.listIterator.asScala.toList.headOption match {
      case Some(doc) => Some(doc.getFieldValue("id").toString.toLong)
      case None => None
    }
  }

  override def addTriple(triple: XTriple, commit: Boolean = true) =
    addTriples(triple :: Nil, commit)

  override def addTriples(triples: Seq[XTriple], commit: Boolean = true) {
    client.add(triples.map(serializeTriple).asJava)
    if (commit) client.commit
  }

  override def updateTripleDpk(triple: XTriple, commit: Boolean = true) =
    addTriples(triple :: Nil, commit)

  override def updateTriplesDpk(triples: Seq[XTriple], commit: Boolean = true) =
    addTriples(triples, commit)

  def read(start: Int, query: String, sorted: Boolean, rows: Int): Seq[XTriple] = {
    val params = new SolrQuery
    params.set("q", query)
    params.setStart(start)
    params.setRows(math.min(rows, 100)) // setting above 100 results in the default value of 10
    if (sorted) params.addSort("id", SolrQuery.ORDER.asc)
    val response = client.query(params)
    (for (doc <- response.getResults.listIterator.asScala) yield deserializeTriple(doc)).toSeq
  }

  override def readTriples(start: Int, sorted: Boolean = false, rows: Int = 100): Seq[XTriple] =
    read(start, allTriplesQuery, sorted, rows)

  override def readTriplesWithDatabasePK(start: Int, sorted: Boolean = false, rows: Int = 100): Seq[XTriple] = {
    val query = "s_avl:* AND -s_dpk:0 AND -o_dpk:0"
    read(start, query, sorted, rows)
  }

  /** Subsequent triples which have the same "subject" or "object" as the provided triple "subject" or "object" */
//  def subsequentLinkedQuery(triple: XTriple) =
//    s"""
//        | id:[${triple.id+1} TO *] AND
//        |  -s_dpk:0 AND -o_dpk:0 AND
//        |  -s_dpk:${triple.s.dpk} AND -o_dpk:${triple.o.dpk} AND
//        | (s_avl:"${escapeChars(triple.s.aval)}" OR
//        |  o_avl:"${escapeChars(triple.s.aval)}" OR
//        |  s_avl:"${escapeChars(triple.o.aval)}" OR
//        |  o_avl:"${escapeChars(triple.o.aval)}")
//      """.stripMargin.trim

  def linkedIdsQuery(aval: String) =
    s"""
        |  -s_dpk:0 AND -o_dpk:0 AND
        | (s_avl:"${escapeChars(aval)}" OR
        |  o_avl:"${escapeChars(aval)}")
      """.stripMargin.trim

  def subsequentLinkedQuery(id: Long, dpk: Long, aval: String) =
    s"""
        | id:[${id+1} TO *] AND
        |  -s_dpk:0 AND -o_dpk:0 AND
        |  -s_dpk:$dpk AND -o_dpk:$dpk AND
        | (s_avl:"${escapeChars(aval)}" OR
        |  o_avl:"${escapeChars(aval)}")
      """.stripMargin.trim

/*
  override def readSubsequentLinkedTriples(start: Int, triple: XTriple, rows: Int = 100, dropLimit: Int = Int.MaxValue): Seq[XTriple] = {
    val sIds = if (count(linkedIdsQuery(triple.s.aval)) <= dropLimit)
      read(start, subsequentLinkedQuery(triple.id, triple.s.dpk, triple.s.aval), sorted = false, rows) else Nil
    val oIds = if (count(linkedIdsQuery(triple.o.aval)) <= dropLimit)
      read(start, subsequentLinkedQuery(triple.id, triple.o.dpk, triple.o.aval), sorted = false, rows) else Nil
    sIds ++ oIds
    //read(start, subsequentLinkedQuery(triple), sorted = false, rows)
  }
*/

  /*
    override def countLinkedTriples(triple: XTriple, dropLimit: Int = Int.MaxValue): Long = {
      val (sCount, oCount) = (count(linkedIdsQuery(triple.s.aval)), count(linkedIdsQuery(triple.o.aval)))
      //(if (sCount <= dropLimit) sCount else 0)+(if (oCount <= dropLimit) oCount else 0)
      //count(subsequentLinkedQuery(triple))
      sCount+oCount
    }
  */

  override def readSubsequentLinkedTriples(start: Int, triple: XTriple, rows: Int = 100, dropLimit: Int = Int.MaxValue): Seq[XTriple] = {
    val sQuery = subsequentLinkedQuery(triple.id, triple.s.dpk, triple.s.aval)
    val oQuery = subsequentLinkedQuery(triple.id, triple.o.dpk, triple.o.aval)
    val sIds = if (count(sQuery) <= dropLimit) read(start, sQuery, sorted = false, rows) else Nil
    val oIds = if (count(oQuery) <= dropLimit) read(start, oQuery, sorted = false, rows) else Nil
    sIds ++ oIds
  }

  override def countLinkedTriples(triple: XTriple, dropLimit: Int = Int.MaxValue): Long = {
    val sQuery = subsequentLinkedQuery(triple.id, triple.s.dpk, triple.s.aval)
    val oQuery = subsequentLinkedQuery(triple.id, triple.o.dpk, triple.o.aval)
    val (sIds, oIds) = (count(sQuery), count(oQuery))
    (if (sIds <= dropLimit) sIds else 0) + (if (oIds <= dropLimit) oIds else 0)
  }

  override def readDistinctSubjects(start: Int, rows: Int = 100): Seq[String] = {
    val params = new SolrQuery
    params.set("q", allTriplesQuery)
    params.setRows(0)
    params.setFacet(true)
    params.addFacetField("s_avl")
    params.setFacetLimit(rows)
    params.setParam(FacetParams.FACET_OFFSET, start.toString)
    val response = client.query(params)
    val field = response.getFacetField("s_avl")
    for (v <- field.getValues.asScala) yield v.getName
  }

  override def readTriplesBySubject(aval: String, rows: Int = 100): Seq[XTriple] =
    read(start = 0, s"""s_avl:"${escapeChars(aval)}"""", sorted = false, rows)

  override def commit = client.commit
  override def optimize = client.optimize
}

@deprecated("Outdated Lucene code fails to compile, not upgraded to newer version", "1.2")
class LuceneIndexClient(path: File) extends IndexClient {
  import IndexToolkit._

  val dir = FSDirectory.open(new File(path, "index"))
  val taxoDir = FSDirectory.open(new File(path, "taxonomy"))
  val analyzer: StandardAnalyzer = null //new StandardAnalyzer(Version.LUCENE_36)
  val config: IndexWriterConfig = null //new IndexWriterConfig(Version.LUCENE_36, analyzer)

  val writer = new IndexWriter(dir, config)
  writer.commit

  val taxoWriter = new DirectoryTaxonomyWriter(taxoDir)
  taxoWriter.commit

  val categoryBuilder = new CategoryDocumentBuilder(taxoWriter)

  var reader: IndexReader = _
  var searcher: IndexSearcher = _

  var taxoReader: DirectoryTaxonomyReader = _

  def createReader {
    reader = IndexReader.open(dir)
    searcher = new IndexSearcher(reader)
    taxoReader = new DirectoryTaxonomyReader(taxoDir)
  }
  createReader

  val allTriplesQuery = NumericRangeQuery.newIntRange("s_typ", 1, 4, true, true)

  override def clear =
    throw new MethodNotSupportedException("clear")

  override def countRecords: Long = writer.numDocs

  def count(query: Query): Long = {
    val collector = new TotalHitCountCollector
    searcher.search(query, collector)
    collector.getTotalHits
  }

  override def countTriples: Long = count(allTriplesQuery)

  def findTripleQuery(triple: XTriple): Query = {
    /*
        s"""s_avl:"${escapeChars(triple.s.aval)}" AND p_avl:"${escapeChars(triple.p.aval)}" AND o_avl:"${escapeChars(triple.o.aval)}""""
    */
    val query = new BooleanQuery
    query.add(new TermQuery(new Term("s_avl", triple.s.aval)), BooleanClause.Occur.MUST)
    query.add(new TermQuery(new Term("p_avl", triple.p.aval)), BooleanClause.Occur.MUST)
    query.add(new TermQuery(new Term("o_avl", triple.o.aval)), BooleanClause.Occur.MUST)
    query
  }

  override def lookupTripleId(triple: XTriple): Option[Long] = {
    val hits = searcher.search(findTripleQuery(triple), 1)
    if (hits.totalHits == 0) None
    else {
      val doc = searcher.doc(hits.scoreDocs(0).doc)
      Some(doc.getFieldable("id").stringValue.toLong)
    }
  }

  implicit class SolrDocToLuceneDoc(solrdoc: SolrInputDocument) {
    def toLucene: Document = {
      val doc = new Document
      solrdoc.getFieldNames.map(name => name -> solrdoc.getFieldValue(name)).foreach { case (k, v) =>
        doc.add(v match {
          case _ if v.isInstanceOf[Long] => new NumericField(k, Field.Store.YES, true).setLongValue(v.asInstanceOf[Long])
          case _ if v.isInstanceOf[Int] => new NumericField(k, Field.Store.YES, true).setIntValue(v.asInstanceOf[Int])
          case _ if v.isInstanceOf[String] => new Field(k, v.asInstanceOf[String], Field.Store.YES, Field.Index.NOT_ANALYZED)
        })
      }
      doc
    }
  }

  implicit class LuceneDocToSolrDoc(doc: Document) {
    def toSolr: SolrDocument =
      new SolrDocument |< { x => doc.getFields.foreach(field => x.addField(field.name, field.stringValue))}
  }

  override def addTriple(triple: XTriple, commit: Boolean = true) =
    addTriples(triple :: Nil, commit)

  override def addTriples(triples: Seq[XTriple], commit: Boolean = true) {
    triples.foreach { triple =>
      val doc = serializeTriple(triple).toLucene
      val category = new CategoryPath("s_avl", doc.getFieldable("s_avl").stringValue)
      categoryBuilder.setCategoryPaths(List(category).asJava)
      categoryBuilder.build(doc)
      writer.deleteDocuments(findTripleQuery(triple))
      writer.addDocument(doc)
    }
    if (commit) {
      writer.commit
      taxoWriter.commit
    }
  }

  override def updateTripleDpk(triple: XTriple, commit: Boolean = true) =
    addTriples(triple :: Nil, commit)

  override def updateTriplesDpk(triples: Seq[XTriple], commit: Boolean = true) =
    addTriples(triples, commit)

  def read(start: Int, query: Query, sorted: Boolean, rows: Int): Seq[XTriple] = {
    val (sort, maxHits) = (new Sort(new SortField("id", SortField.LONG)), start+rows)
    try {
      val hits = if (sorted) searcher.search(query, maxHits, sort) else searcher.search(query, maxHits)
      (start until math.min(hits.totalHits, maxHits)).map(n =>
        deserializeTriple(searcher.doc(hits.scoreDocs(n).doc).toSolr))
    } catch {
      case e: IllegalArgumentException => Nil // numHits must be > 0, please use ...
    }
  }

  override def readTriples(start: Int, sorted: Boolean = false, rows: Int = 100): Seq[XTriple] =
    read(start, allTriplesQuery, sorted, rows)

  override def readTriplesWithDatabasePK(start: Int, sorted: Boolean = false, rows: Int = 100): Seq[XTriple] = {
    /*
        "s_avl:* AND -s_dpk:0 AND -o_dpk:0"
    */
    val query = new BooleanQuery
    query.add(allTriplesQuery, BooleanClause.Occur.MUST)
    query.add(NumericRangeQuery.newLongRange("s_dpk", 0L, 0L, true, true), BooleanClause.Occur.MUST_NOT) // -s_dpk:0
    query.add(NumericRangeQuery.newLongRange("o_dpk", 0L, 0L, true, true), BooleanClause.Occur.MUST_NOT) // -o_dpk:0
    read(start, query, sorted, rows)
  }

  /** Subsequent triples which have the same "subject" or "object" as the provided triple "subject" or "object" */
  def subsequentLinkedQuery(triple: XTriple): Query = {
    /*
        s"""
            | id:[${triple.id+1} TO *] AND
            |  -s_dpk:0 AND -o_dpk:0 AND
            |  -s_dpk:${triple.s.dpk} AND -o_dpk:${triple.o.dpk} AND
            | (s_avl:${escapeChars(triple.s.aval)} OR
            |  o_avl:${escapeChars(triple.s.aval)} OR
            |  s_avl:${escapeChars(triple.o.aval)} OR
            |  o_avl:${escapeChars(triple.o.aval)})
          """.stripMargin.trim
    */
    val query = new BooleanQuery
    query.add(NumericRangeQuery.newLongRange("id", triple.id+1, Long.MaxValue, true, true), BooleanClause.Occur.MUST)
    query.add(NumericRangeQuery.newLongRange("s_dpk", 0L, 0L, true, true), BooleanClause.Occur.MUST_NOT)
    query.add(NumericRangeQuery.newLongRange("o_dpk", 0L, 0L, true, true), BooleanClause.Occur.MUST_NOT)
    query.add(NumericRangeQuery.newLongRange("s_dpk", triple.s.dpk, triple.s.dpk, true, true), BooleanClause.Occur.MUST_NOT)
    query.add(NumericRangeQuery.newLongRange("o_dpk", triple.o.dpk, triple.o.dpk, true, true), BooleanClause.Occur.MUST_NOT)

    val orQuery = new BooleanQuery
    orQuery.add(new TermQuery(new Term("s_avl", triple.s.aval)), BooleanClause.Occur.SHOULD)
    orQuery.add(new TermQuery(new Term("o_avl", triple.s.aval)), BooleanClause.Occur.SHOULD)
    orQuery.add(new TermQuery(new Term("s_avl", triple.o.aval)), BooleanClause.Occur.SHOULD)
    orQuery.add(new TermQuery(new Term("o_avl", triple.o.aval)), BooleanClause.Occur.SHOULD)

    query.add(orQuery, BooleanClause.Occur.MUST)
    query
  }

  def linkedIdsQuery(aval: String): Query = {
/*
    s"""
        |  -s_dpk:0 AND -o_dpk:0 AND
        | (s_avl:"${escapeChars(aval)}" OR
        |  o_avl:"${escapeChars(aval)}")
      """.stripMargin.trim
*/
    val query = new BooleanQuery
    query.add(NumericRangeQuery.newLongRange("s_dpk", 0L, 0L, true, true), BooleanClause.Occur.MUST_NOT)
    query.add(NumericRangeQuery.newLongRange("o_dpk", 0L, 0L, true, true), BooleanClause.Occur.MUST_NOT)

    val orQuery = new BooleanQuery
    orQuery.add(new TermQuery(new Term("s_avl", aval)), BooleanClause.Occur.SHOULD)
    orQuery.add(new TermQuery(new Term("o_avl", aval)), BooleanClause.Occur.SHOULD)

    query.add(orQuery, BooleanClause.Occur.MUST)
    query
  }

  def subsequentLinkedQuery(id: Long, dpk: Long, aval: String): Query = {
/*
    s"""
        | id:[${id+1} TO *] AND
        |  -s_dpk:0 AND -o_dpk:0 AND
        |  -s_dpk:$dpk AND -o_dpk:$dpk AND
        | (s_avl:"${escapeChars(aval)}" OR
        |  o_avl:"${escapeChars(aval)}")
      """.stripMargin.trim
*/
    val query = new BooleanQuery
    query.add(NumericRangeQuery.newLongRange("id", id+1, Long.MaxValue, true, true), BooleanClause.Occur.MUST)
    query.add(NumericRangeQuery.newLongRange("s_dpk", 0L, 0L, true, true), BooleanClause.Occur.MUST_NOT)
    query.add(NumericRangeQuery.newLongRange("o_dpk", 0L, 0L, true, true), BooleanClause.Occur.MUST_NOT)
    query.add(NumericRangeQuery.newLongRange("s_dpk", dpk, dpk, true, true), BooleanClause.Occur.MUST_NOT)
    query.add(NumericRangeQuery.newLongRange("o_dpk", dpk, dpk, true, true), BooleanClause.Occur.MUST_NOT)

    val orQuery = new BooleanQuery
    orQuery.add(new TermQuery(new Term("s_avl", aval)), BooleanClause.Occur.SHOULD)
    orQuery.add(new TermQuery(new Term("o_avl", aval)), BooleanClause.Occur.SHOULD)

    query.add(orQuery, BooleanClause.Occur.MUST)
    query
  }

/*
  override def readSubsequentLinkedTriples(start: Int, triple: XTriple, rows: Int = 100, dropLimit: Int = Int.MaxValue): Seq[XTriple] = {
    val sIds = if (count(linkedIdsQuery(triple.s.aval)) <= dropLimit)
      read(start, subsequentLinkedQuery(triple.id, triple.s.dpk, triple.s.aval), sorted = false, rows) else Nil
    val oIds = if (count(linkedIdsQuery(triple.o.aval)) <= dropLimit)
      read(start, subsequentLinkedQuery(triple.id, triple.o.dpk, triple.o.aval), sorted = false, rows) else Nil
    sIds ++ oIds
    //read(start, subsequentLinkedQuery(triple), sorted = false, rows)
  }
*/

  /*
    override def countLinkedTriples(triple: XTriple, dropLimit: Int = Int.MaxValue): Long = {
      val (sCount, oCount) = (count(linkedIdsQuery(triple.s.aval)), count(linkedIdsQuery(triple.o.aval)))
      //(if (sCount <= dropLimit) sCount else 0)+(if (oCount <= dropLimit) oCount else 0)
      //count(subsequentLinkedQuery(triple))
      sCount+oCount
    }
  */

  override def readSubsequentLinkedTriples(start: Int, triple: XTriple, rows: Int = 100, dropLimit: Int = Int.MaxValue): Seq[XTriple] = {
    val sQuery = subsequentLinkedQuery(triple.id, triple.s.dpk, triple.s.aval)
    val oQuery = subsequentLinkedQuery(triple.id, triple.o.dpk, triple.o.aval)
    val sIds = if (count(sQuery) <= dropLimit) read(start, sQuery, sorted = false, rows) else Nil
    val oIds = if (count(oQuery) <= dropLimit) read(start, oQuery, sorted = false, rows) else Nil
    sIds ++ oIds
  }

  override def countLinkedTriples(triple: XTriple, dropLimit: Int = Int.MaxValue): Long = {
    val sQuery = subsequentLinkedQuery(triple.id, triple.s.dpk, triple.s.aval)
    val oQuery = subsequentLinkedQuery(triple.id, triple.o.dpk, triple.o.aval)
    val (sIds, oIds) = (count(sQuery), count(oQuery))
    (if (sIds <= dropLimit) sIds else 0) + (if (oIds <= dropLimit) oIds else 0)
  }

  override def readDistinctSubjects(start: Int, rows: Int = 100): Seq[String] = {
    val params = new FacetSearchParams
    params.addFacetRequest(new CountFacetRequest(new CategoryPath("s_avl"), Int.MaxValue))
    val collector = new FacetsCollector(params, reader, taxoReader)
    searcher.search(allTriplesQuery, MultiCollector.wrap(new TotalHitCountCollector, collector))
    (for (result <- collector.getFacetResults) yield {
      val facets = result.getFacetResultNode.getSubResults.asScala.toList
      (start until math.min(facets.size, start+rows)).map(n => facets(n).getLabel.lastComponent)
    }).flatten
  }

  override def readTriplesBySubject(aval: String, rows: Int = 100): Seq[XTriple] =
    read(start = 0, new TermQuery(new Term("s_avl", aval)), sorted = false, rows)

  override def commit {
    writer.commit
    taxoWriter.commit
  }

  override def optimize {
    writer.close
    taxoWriter.close
    createReader
  }
}

object MemdbIndexClient {
  val cacheFolder = "memdb-index"

  class TripleCache(cacheId: String) {
    private val cache = DbMap.createMap[Long,XTriple](cacheFolder, cacheId+"-c") // triple.id -> triple
    private val indexCache = DbMap.createMap[Int,Long](cacheFolder, cacheId+"-i") // index 0..n -> triple.id
    private var lastTripleId = 0L
    private val threshold = 10000

    private var index = indexCache.size
    def size = index

    // accumulate writes before spill data to disk
    private val writeMap = new mutable.HashMap[Long,XTriple]
    private val writeIndex = new mutable.HashMap[Int,Long]

    def add(triple: XTriple) {
      assert(triple.id > lastTripleId, "Cache ordered is invalid") // sorted by ID
      lastTripleId = triple.id

      if (writeMap.size >= threshold) commit
      writeMap.put(triple.id, triple)
      writeIndex.put(index, triple.id)
      index += 1
    }

    /** called to update triple database PK while readMap is used but the updated triple is not needed by it */
    def update(triple: XTriple) = {
      if (writeMap.size >= threshold) commit
      writeMap.put(triple.id, triple)
      readMap.invalidate(triple.id)
    }

    def commit {
      cache.putAll(writeMap.asJava)
      indexCache.putAll(writeIndex.asJava)
      writeMap.clear
      writeIndex.clear
    }

    // accumulate reads (by the time the reader is used all writes were committed, no writeMap lookups needed)
    private val gcBuilder = CacheBuilder.newBuilder.maximumSize(threshold)
    private val readMap = gcBuilder.asInstanceOf[CacheBuilder[Long,XTriple]].build[Long,XTriple]
    private val readIndex = gcBuilder.asInstanceOf[CacheBuilder[Int,Long]].build[Int,Long]

    def getByIndex(n: Int): XTriple = {
      val tripleId = Option(readIndex.getIfPresent(n)) match {
        case Some(id) => id // found in readMap
        case None =>
          val id = indexCache.get(n) // found in cache, move to readMap
          readIndex.put(n, id)
          id
      }
      Option(readMap.getIfPresent(tripleId)) match {
        case Some(triple) => triple // found in readMap
        case None =>
          val triple = cache.get(tripleId) // found in cache, move to readMap
          readMap.put(tripleId, triple)
          triple
      }
    }

    def get(id: Long): XTriple =
      Option(readMap.getIfPresent(id)) match {
        case Some(triple) => triple // found in readMap
        case None =>
          val triple = cache.get(id) // found in cache, move to readMap
          readMap.put(id, triple)
          triple
      }
  }

  class BySubjectCache(cacheId: String) {
    private val cache = DbMap.createMap[String,mutable.ListBuffer[Long]](cacheFolder, cacheId+"-c") // triple.s.aval -> cache with triple.id
    private val indexCache = DbMap.createMap[Int,String](cacheFolder, cacheId+"-i") // index 0..n -> triple.s.aval
    private val threshold = 10000

    private var index = indexCache.size
    def size = index

    // accumulate writes before spill data to disk
    private val writeMap = new mutable.HashMap[String,mutable.ListBuffer[Long]]
    private val writeIndex = new mutable.HashMap[Int,String]

    def add(triple: XTriple) = {
      if (writeMap.size >= threshold) commit
      val tripleIds = writeMap.get(triple.s.aval) match {
        case Some(ids) =>
          assert(triple.id > ids.last, "Cache ordered is invalid") // sorted by ID
          ids
        case None =>
          val ids = getFromCache(triple.s.aval)
          if (ids.isEmpty) {
            writeIndex.put(index, triple.s.aval)
            index += 1
          }
          writeMap.put(triple.s.aval, ids)
          ids
      }
      tripleIds += triple.id
    }

    private def getFromCache(aval: String): mutable.ListBuffer[Long] =
      Option(cache.get(aval)) match {
        case Some(ids) => ids
        case None => new mutable.ListBuffer[Long]
      }

    def commit {
      cache.putAll(writeMap.asJava)
      indexCache.putAll(writeIndex.asJava)
      writeMap.clear
      writeIndex.clear
    }

    // accumulate reads (by the time the reader is used all writes were committed, no writeMap lookups needed)
    private val gcBuilder = CacheBuilder.newBuilder.maximumSize(threshold)
    private val readMap = gcBuilder.asInstanceOf[CacheBuilder[String,mutable.ListBuffer[Long]]].build[String,mutable.ListBuffer[Long]]
    private val readIndex = gcBuilder.asInstanceOf[CacheBuilder[Int,String]].build[Int,String]

    def get(aval: String): mutable.ListBuffer[Long] =
      Option(readMap.getIfPresent(aval)) match {
        case Some(ids) => ids // found in readMap
        case None =>
          val ids = cache.get(aval) // found in cache, move to readMap
          readMap.put(aval, ids)
          ids
      }

    def getSubjectByIndex(n: Int): String =
      Option(readIndex.getIfPresent(n)) match {
        case Some(subject) => subject // found in readMap
        case None =>
          val subject = indexCache.get(n) // found in cache, move to readMap
          readIndex.put(n, subject)
          subject
      }
  }

  class LinkedTriplesCache(cacheId: String) {
    private val cache = DbMap.createMap[String,mutable.ListBuffer[Long]](cacheFolder, cacheId+"-c") // aval -> cache with triple.id
    private val indexCache = DbMap.createMap[Int,String](cacheFolder, cacheId+"-i") // index 0..n -> aval
    private val threshold = 10000

    private var index = indexCache.size
    def size = index

    // accumulate writes before spill data to disk
    private val writeMap = new mutable.HashMap[String,mutable.ListBuffer[Long]]
    private val writeIndex = new mutable.HashMap[Int,String]

    def add(aval: String, triple: XTriple) = {
      if (writeMap.size >= threshold) commit
      val tripleIds = writeMap.get(aval) match {
        case Some(ids) =>
          assert(triple.id > ids.last, "Cache ordered is invalid") // sorted by ID
          ids
        case None =>
          val ids = getFromCache(aval)
          if (ids.isEmpty) {
            writeIndex.put(index, aval)
            index += 1
          }
          writeMap.put(aval, ids)
          ids
      }
      tripleIds += triple.id
    }

    private def getFromCache(aval: String) =
      Option(cache.get(aval)) match {
        case Some(ids) => ids
        case None => new mutable.ListBuffer[Long]
      }

    def commit {
      cache.putAll(writeMap.asJava)
      indexCache.putAll(writeIndex.asJava)
      writeMap.clear
      writeIndex.clear
    }

    // accumulate reads (by the time the reader is used all writes were committed, no writeMap lookups needed)
    private val gcBuilder = CacheBuilder.newBuilder.maximumSize(threshold)
    private val readMap = gcBuilder.asInstanceOf[CacheBuilder[String,mutable.ListBuffer[Long]]].build[String,mutable.ListBuffer[Long]]

    private val emptyBuffer = new mutable.ListBuffer[Long]

    def getIds(aval: String): mutable.ListBuffer[Long] =
      Option(readMap.getIfPresent(aval)) match {
        case Some(ids) => ids // found in readMap
        case None =>
          val ids = Option(cache.get(aval)) match { // found in cache (may not exist), move to readMap
            case Some(xs) => xs
            case _ => emptyBuffer
          }
          readMap.put(aval, ids)
          ids
      }
  }

  // shared cache with triples
  val tripleCache = new TripleCache("triple")

  // shared cache with triples grouped by subject
  val bySubjectCache = new BySubjectCache("bysub")

  // shared cache with triples with the same subject / object
  val linkedSubjectCache = new LinkedTriplesCache("linksub")
  val linkedObjectCache = new LinkedTriplesCache("linkobj")
}

class MemdbIndexClient extends IndexClient {
  import MemdbIndexClient._

  override def clear =
    throw new MethodNotSupportedException("clear")

  override def countRecords: Long = countTriples

  override def countTriples: Long = tripleCache.size

  override def lookupTripleId(triple: XTriple): Option[Long] =
    throw new MethodNotSupportedException("lookupTripleId")

  override def addTriple(triple: XTriple, commit: Boolean = true) =
    addTriples(triple :: Nil, commit)

  override def addTriples(triples: Seq[XTriple], commit: Boolean = true) = {
    triples.foreach { triple =>
      tripleCache.add(triple)
      bySubjectCache.add(triple)
      linkedSubjectCache.add(triple.s.aval, triple)
      linkedObjectCache.add(triple.o.aval, triple)
    }
  }

  override def updateTripleDpk(triple: XTriple, commit: Boolean = true) =
    updateTriplesDpk(triple :: Nil, commit)

  override def updateTriplesDpk(triples: Seq[XTriple], commit: Boolean = true) = {
    triples.foreach { triple =>
      val cached = tripleCache.get(triple.id)
      cached.s.dpk = triple.s.dpk
      cached.o.dpk = triple.o.dpk
      tripleCache.update(cached)
    }
  }

  override def readTriples(start: Int, sorted: Boolean = false, rows: Int = 100): Seq[XTriple] =
    (start until math.min(start+rows, tripleCache.size)).map(n => tripleCache.getByIndex(n))

  override def readTriplesWithDatabasePK(start: Int, sorted: Boolean = false, rows: Int = 100): Seq[XTriple] =
    (start until math.min(start+rows, tripleCache.size)).map(n => tripleCache.getByIndex(n))

  def linkedIds(aval: String): List[mutable.ListBuffer[Long]] =
    linkedSubjectCache.getIds(aval) :: linkedObjectCache.getIds(aval) :: Nil

  private def readSubsequentLinkedTriples(start: Int, triple: XTriple, rows: Int, dropLimit: Int, f: (Long) => XTriple): Seq[XTriple] = {
    val emptyBuffer = new mutable.ListBuffer[Long]
    val (sIds, oIds) = (linkedIds(triple.s.aval), linkedIds(triple.o.aval))

    def subsequestIds(ids: List[mutable.ListBuffer[Long]]): List[mutable.ListBuffer[Long]] =
      if (ids.map(_.size).sum <= dropLimit) ids.map(xs => xs.filter(id => triple.id > id && id != 0))
      else List(emptyBuffer, emptyBuffer)

    val ids4 = subsequestIds(sIds) ::: subsequestIds(oIds) // 4 slots with IDs
    val ids4size = ids4.map(_.size) // slots sizes
    val ids4incs = (0 until 4).map(i => (0 to i).map(ids4size(_)).sum).zipWithIndex // slots incremental sizes (size1, size1+size2 etc)

    (start until math.min(rows, ids4size.sum)).map { n =>
      val i = ids4incs.find(n < _._1).get._2 // I is the slot index [0 .. 4)
      val j = n - (if (i == 0) 0 else ids4incs(i-1)._1) // J is the index in the slot [0 .. slot size)
      f(ids4(i)(j))
    }
  }

  override def readSubsequentLinkedTriples(start: Int, triple: XTriple, rows: Int = 100, dropLimit: Int = Int.MaxValue): Seq[XTriple] = {
    val f = (tripleId: Long) => tripleCache.get(tripleId)
    readSubsequentLinkedTriples(start, triple, rows, dropLimit, f)
  }

  override def countLinkedTriples(triple: XTriple, dropLimit: Int = Int.MaxValue): Long = {
    val dummyTriple = new XTriple(AnonNode(""), AnonNode(""), AnonNode(""))
    val f = (tripleId: Long) => dummyTriple
    val n = readSubsequentLinkedTriples(0, triple, Int.MaxValue, dropLimit, f).size
    if (n <= dropLimit) n else 0
  }

/*
  override def countLinkedTriples(triple: XTriple, dropLimit: Int = Int.MaxValue): Long = {
    val (sCount, oCount) = (linkedIds(triple.s.aval).map(_.size).sum, linkedIds(triple.o.aval).map(_.size).sum)
    //(if (sCount <= dropLimit) sCount else 0)+(if (oCount <= dropLimit) oCount else 0)
    sCount+oCount
  }
*/

  override def readDistinctSubjects(start: Int, rows: Int = 100): Seq[String] =
    (start until math.min(start + rows, bySubjectCache.size)).map(n => bySubjectCache.getSubjectByIndex(n))

  override def readTriplesBySubject(aval: String, rows: Int = 100): Seq[XTriple] = {
    val ids = bySubjectCache.get(aval)
    ids.take(math.min(rows, ids.size)).map(id => tripleCache.get(id))
  }

  override def commit {
    tripleCache.commit
    bySubjectCache.commit
    linkedSubjectCache.commit
    linkedObjectCache.commit
  }

  override def optimize {}

}
