package com.coldcore.rdf2neo

class ConfigParamNotFoundException(name: String) extends RuntimeException(s"Configuration parameter $name not found")

class InvalidConfigParamException(key: String, value: String) extends RuntimeException(s"Invalid configuration parameter $key=$value")

class CannotDeletePathException(path: String) extends RuntimeException(s"Cannot delete $path")

class MarkerFileNotFoundException(path: String) extends RuntimeException(s"Cannot find marker file $path")

class CannotReadConfigException(filename: String) extends RuntimeException(s"Cannot read configuration file $filename")
