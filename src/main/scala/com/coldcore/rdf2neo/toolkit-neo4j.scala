package com.coldcore.rdf2neo
package toolkit
package neo4j

import java.sql.SQLException
import java.util.concurrent.locks.ReentrantLock
import scala.collection.JavaConverters._
import com.coldcore.rdf2neo.neo4j.contrib.jdbc.CypherExecutor
import org.neo4j.unsafe.batchinsert.BatchInserters
import java.io.File
import org.neo4j.graphdb.{DynamicRelationshipType, DynamicLabel}
import org.apache.http.MethodNotSupportedException
import scala.collection.mutable
import com.google.common.cache.CacheBuilder
import java.net.URLDecoder

object Neo4jToolkit {

  val nodeProps = (node: XNode) => {
/* if "-" is used instead of "%" as in Sp%C3%A9cial:URIResolver/Cat-C3-A9gorie-3APok-C3-A9mon_du_groupe_Insecte
    val decode = (x: String) => {
      val regex = "[-]{1}[0-9A-F]{2}"
      val pattern = Pattern.compile(regex)
      val matcher = pattern.matcher(x)
      var s = x
      while (matcher.find) s = s.substring(0, matcher.start)+"%"+s.substring(matcher.start+1)
      URLDecoder.decode(s, "UTF-8")
    }
*/
    val decode = (x: String) => URLDecoder.decode(x, "UTF-8")
    Map(
      "aval" -> node.aval,
      "uuid" -> node.uuid,
      "ntype" -> node.`type`.toString) ++
      (node match {
        case TypeLiteralNode(value, datatypeUri) =>
          Map(
            "value" -> value,
            "dturi" -> datatypeUri)
        case PlainLiteralNode(value, language) =>
          Map(
            "value" -> value,
            "lng" -> language)
        case un@UriNode(uri) =>
          Map(
            "uri" -> decode(uri),
            "value" -> decode(un.value))
        case AnonNode(idval) =>
          Map(
            "idval" -> idval)
      }).toMap
  }

  val relationshipType = (node: XNode) => {
    val value = node match {
      case TypeLiteralNode(value, datatypeUri) => value
      case PlainLiteralNode(value, language) => value
      case un@UriNode(uri) => un.value
      case AnonNode(idval) => idval
    }
    val x = value.map(c => if (c.toString.matches("[a-zA-z0-9]")) c else '_').mkString
    if (x.head.toString.matches("[a-zA-z]")) x else "_"+x
  }

  val nodeTypeString = (node: XNode) =>
    node match {
      case TypeLiteralNode(value, datatypeUri) => "TypeLiteral"
      case PlainLiteralNode(value, language) => "PlainLiteral"
      case un@UriNode(uri) => "URI"
      case AnonNode(idval) => "Anon"
    }
}

sealed trait Neo4jClientMode
case object DiskNeo4jClientMode extends Neo4jClientMode
case object DiskFcNeo4jClientMode extends Neo4jClientMode
case object JdbcFcNeo4jClientMode extends Neo4jClientMode

trait DbClient {
  def countNodes: Long
  def clear
  def addTriple(triple: XTriple)
  def addSubjectTriples(triples: Seq[XTriple])
  def addNode(node: XNode): Long
  def addRelationship(nodeA: Long, nodeB: Long, relationshipNode: XNode)
  def addLinkRelationship(nodeA: Long, nodeB: Long, relationshipNode: XNode)
  def nodeExists(nodeId: Long): Boolean
  def close
  def linkNodes(nodes: Seq[XNode], relationshipNode: XNode): Int
  def linkNodesToAnon(nodes: Seq[XNode], relationshipNode: XNode): Int
}

trait DbClientActions {
  self: DbClient =>

  val emptyCypherMap = Map.empty[String, AnyRef].asJava
  val cypherMap = (map: Map[String, AnyRef]) => map.asJava

  def doAddSubjectTriples(triples: Seq[XTriple]) {
    val nodeA = addNode(triples.head.s)
    triples.foreach { triple =>
      val nodeB = addNode(triple.o)
      triple.s.dpk = nodeA
      triple.o.dpk = nodeB
      addRelationship(nodeA, nodeB, triple.p)
    }
  }

  def doLinkNodes(nodeIds: Seq[Long], relationshipNode: XNode): Int =
    if (nodeIds.size < 2) 0
    else {
      val znodes = nodeIds.zipWithIndex
      var links = 0
      for ((nodeA, indexA) <- znodes; (nodeB, indexB) <- znodes if indexA < indexB) {
        addLinkRelationship(nodeA, nodeB, relationshipNode)
        links += 1
      }
      links
    }

  def doLinkNodesToAnon(nodeIds: Seq[Long], relationshipNode: XNode, anonId: Long): Int =
    if (nodeIds.isEmpty) 0
    else {
      var links = 0
      nodeIds.foreach { nodeId =>
        addLinkRelationship(nodeId, anonId, relationshipNode)
        links += 1
      }
      links
    }
}

object DbCache {
  val cacheFolder = "neo4j-cache"

  class AnonPkCache(cacheId: String) {
    private val threshold = 10000
    private val cache = DbMap.createMap[String,Long](cacheFolder, cacheId) // aval -> node ID

    // accumulate writes before spill data to disk
    private val writeMap = new mutable.HashMap[String,Long]

    def add(aval: String, nodeId: Long) = {
      if (writeMap.size >= threshold) commit
      writeMap.put(aval, nodeId)
      readMap.invalidate(aval)
    }

    // accumulate reads
    private val gcBuilder = CacheBuilder.newBuilder.maximumSize(threshold)
    private val readMap = gcBuilder.asInstanceOf[CacheBuilder[String,Long]].build[String,Long]

    def get(aval: String): Option[Long] =
      Option(readMap.getIfPresent(aval)) match {
        case x @ Some(_) => x // found in readMap
        case None =>
          writeMap.get(aval) match {
            case x @ Some(_) => x // found in writeMap, no need to move into readMap
            case None =>
              Option(cache.get(aval)) match {
                case x @ Some(id) => readMap.put(aval, id); x // found in cache, move to readMap
                case None => None
              }
          }
      }

    def commit {
      cache.putAll(writeMap.asJava)
      writeMap.clear
    }
  }

  class NodesCache(cacheId: String) {
    private val threshold = 10000
    val cache = DbMap.createMap[Long,Long](cacheFolder, cacheId) // node ID -> node ID

    // accumulate writes before spill data to disk
    private val writeMap = new mutable.HashMap[Long,Long]

    def add(nodeId: Long) = {
      if (writeMap.size >= threshold) commit
      writeMap.put(nodeId, nodeId)
      readMap.invalidate(nodeId)
    }

    // accumulate reads
    private val gcBuilder = CacheBuilder.newBuilder.maximumSize(threshold)
    private val readMap = gcBuilder.asInstanceOf[CacheBuilder[Long,Long]].build[Long,Long]

    def get(nodeId: Long): Option[Long] =
      Option(readMap.getIfPresent(nodeId)) match {
        case x @ Some(_) => x // found in readMap
        case None =>
          writeMap.get(nodeId) match {
            case x @ Some(_) => x // found in writeMap, no need to move into readMap
            case None =>
              Option(cache.get(nodeId)) match {
                case x @ Some(id) => readMap.put(nodeId, id); x // found in cache, move to readMap
                case None => None
              }
          }
      }

    def commit {
      cache.putAll(writeMap.asJava)
      writeMap.clear
    }
  }

  class NodeLinkCache(cacheId: String) {
    private val threshold = 10000
    val cache = DbMap.createMap[Long,mutable.HashSet[Long]](cacheFolder, cacheId) // node ID -> linked node IDs

    // accumulate writes before spill data to disk
    private val writeMap = new mutable.HashMap[Long,mutable.HashSet[Long]]

    def add(id: Long, id2: Long) = {
      if (writeMap.size >= threshold) commit
      val nodeIds = writeMap.get(id) match {
        case Some(ids) => ids
        case None =>
          val ids = getFromCache(id)
          writeMap.put(id, ids)
          ids
      }
      nodeIds += id2
      readMap.invalidate(id)
    }

    private def getFromCache(nodeId: Long) =
      Option(cache.get(nodeId)) match {
        case Some(ids) => ids
        case None => new mutable.HashSet[Long]
      }

    // accumulate reads
    private val gcBuilder = CacheBuilder.newBuilder.maximumSize(threshold)
    private val readMap = gcBuilder.asInstanceOf[CacheBuilder[Long,mutable.HashSet[Long]]].build[Long,mutable.HashSet[Long]]

    def get(nodeId: Long): Option[mutable.HashSet[Long]] =
      Option(readMap.getIfPresent(nodeId)) match {
        case Some(x) => Some(x) // found in readMap
        case None =>
          writeMap.get(nodeId) match {
            case Some(x) => Some(x) // found in writeMap, no need to move into readMap
            case None =>
              Option(cache.get(nodeId)) match {
                case Some(x) => readMap.put(nodeId, x); Some(x) // found in cache, move to readMap
                case None => None
              }
          }
      }

    def commit {
      cache.putAll(writeMap.asJava)
      writeMap.clear
    }
  }

  class ProcessedDpkCache {
    private val threshold = 10000
    val cache = DbMap.createMap[Long,Long]("tmp") // node database PK -> node database PK

    // accumulate writes before spill data to disk
    private val writeMap = new mutable.HashMap[Long,Long]

    def add(dpk: Long) = {
      if (writeMap.size >= threshold) commit
      writeMap.put(dpk, dpk)
      readMap.invalidate(dpk)
    }

    // accumulate reads
    private val gcBuilder = CacheBuilder.newBuilder.maximumSize(threshold)
    private val readMap = gcBuilder.asInstanceOf[CacheBuilder[Long,Long]].build[Long,Long]

    def get(dpk: Long): Option[Long] =
      Option(readMap.getIfPresent(dpk)) match {
        case x @ Some(_) => x // found in readMap
        case None =>
          writeMap.get(dpk) match {
            case x @ Some(_) => x // found in writeMap, no need to move into readMap
            case None =>
              Option(cache.get(dpk)) match {
                case x @ Some(id) => readMap.put(dpk, id); x // found in cache, move to readMap
                case None => None
              }
          }
      }

    def commit {
      cache.putAll(writeMap.asJava)
      writeMap.clear
    }
  }

  // shared cache with primary keys of anon nodes and its lock
  val (anonPkCache, anonPkLock) = (new AnonPkCache("anonpk"), new ReentrantLock)

  // shared cache with node links and its lock
  val (nodeLinkCache, nodeLinkLock) = (new NodeLinkCache("nodelink"), new ReentrantLock)

  // shared cache with all nodes
  val nodesCache = new NodesCache("nodes")

  // shared cache with processed database PK
  val processedDpkCache = new ProcessedDpkCache
}

trait DbCacheActions {
  self: DbClient =>
  import DbCache._

  def getAnonId(node: XNode, linksPerNode: Int, linkNodes: (Long,Long) => Unit): Long = {
    val newLinkedAnonNode = (aval: String, linkTo: Long) => {
      val id = addNode(AnonNode(aval))
      linkNodes(id, linkTo)
      id
    }
    anonPkLock.lock
    val anonId = anonPkCache.get(node.aval) match {
      case Some(pk) =>
        val nodeId =
          if (countLinksOnNode(pk) < linksPerNode-1) pk // can handle more links
          else newLinkedAnonNode(node.aval, pk) // exceeded - create a new and link to this
        anonPkCache.add(node.aval, nodeId)
        nodeId
      case None =>
        val nodeId =
          node match {
            case AnonNode(_) if countLinksOnNode(node.dpk) < linksPerNode-1 => node.dpk // can handle more links
            case AnonNode(_) => newLinkedAnonNode(node.aval, node.dpk) // exceeded - create a new and link to this
            case _ => addNode(AnonNode(node.aval)) // create a new
          }
        anonPkCache.add(node.aval, nodeId)
        nodeId
    }
    anonPkLock.unlock
    anonId
  }

  def cacheLinkRelationship(nodeA: Long, nodeB: Long): Boolean = {
    val exists = (id: Long, id2: Long) => nodeLinkCache.get(id) match {
      case Some(ids) if ids.contains(id2) => true
      case _ => nodeLinkCache.add(id, id2); false
    }
    nodeLinkLock.lock
    val existsA = exists(nodeA, nodeB)
    val existsB = exists(nodeB, nodeA)
    nodeLinkLock.unlock
    existsA || existsB
  }

  def countLinksOnNode(nodeId: Long): Int = {
    nodeLinkLock.lock
    val count = nodeLinkCache.get(nodeId) match {
      case Some(ids) => ids.size
      case None => 0
    }
    nodeLinkLock.unlock
    count
  }

  def cacheProcessedDpk(dpk: Long): Boolean = {
    processedDpkCache.get(dpk) match {
      case Some(_) => true
      case _ => processedDpkCache.add(dpk); false
    }
  }

  def closeCache {
    nodeLinkLock.lock
    nodeLinkCache.commit
    nodeLinkLock.unlock

    anonPkLock.lock
    anonPkCache.commit
    anonPkLock.unlock

    nodesCache.commit

    processedDpkCache.commit
  }
}

/** Uses links cache and anon cache */
class JdbcDbClient(executor: CypherExecutor, linksPerNode: Int) extends DbClient with DbClientActions with DbCacheActions {
  import Neo4jToolkit._

  override def countNodes: Long = {
    val query = "start n=node(*) match n return count(n)"
    val result = executor.query(query, emptyCypherMap).asScala.toList
    val count = result.head.asScala.get("count(n)").get.toString.toLong
    count
  }

  override def clear {
    val (rquery, nquery) = ("MATCH ()-[r]-() DELETE r", "MATCH (n) DELETE n")
    executor.query(rquery, emptyCypherMap)
    executor.query(nquery, emptyCypherMap)
    assert(countNodes == 0)
  }

  override def addTriple(triple: XTriple) =
    addSubjectTriples(triple :: Nil)

  override def addSubjectTriples(triples: Seq[XTriple]) =
    doAddSubjectTriples(triples)

  override def addNode(node: XNode): Long = {
    val (ntype, props) = (nodeTypeString(node), nodeProps(node).zipWithIndex)
    val query = s"CREATE (n:$ntype { ${props.map { case ((k,v),n) => k+":{"+n+"}" }.mkString(",")} }) RETURN id(n)"
    val cymap = props.map { case ((k, v), n) => n.toString -> v }.toMap
    val result = executor.query(query, cypherMap(cymap)).asScala.toList
    result.head.asScala.get("id(n)").get.toString.toLong
  }

  override def addRelationship(nodeA: Long, nodeB: Long, relationshipNode: XNode) = {
    val (rtype, props) = (relationshipType(relationshipNode), nodeProps(relationshipNode).zipWithIndex)
    val query = s"""
      | MATCH (a),(b)
      | WHERE id(a)=$nodeA AND id(b)=$nodeB
      | CREATE (a)-[r:$rtype { ${props.map { case ((k,v),n) => k+":{"+n+"}" }.mkString(",")} }]->(b)
      """.stripMargin.trim
    val cymap = props.map { case ((k, v), n) => n.toString -> v }.toMap
    val (failRetry, failSleep) = (10, 100L) // retries on commit fail (conflict with other writers) and sleep time
    (true /: (1 to failRetry))((a, _) =>
      try { if (a) executor.query(query, cypherMap(cymap)); false } catch {
        case e: RuntimeException if e.getCause.isInstanceOf[SQLException] => Thread.sleep(failSleep); true })
  }

/*
  def hasRelationship(nodeA: Long, nodeB: Long, relationshipNode: XNode): Boolean = {
    val rtype = relationshipType(relationshipNode)
    val query = s"MATCH (a)-[r:$rtype]-(b) WHERE id(a)=$nodeA AND id(b)=$nodeB RETURN id(a)"
    val result = executor.query(query, emptyCypherMap).asScala.toList
    result.nonEmpty
  }
*/

  override def addLinkRelationship(nodeA: Long, nodeB: Long, relationshipNode: XNode) =
    if (!cacheLinkRelationship(nodeA, nodeB)) addRelationship(nodeA, nodeB, relationshipNode)

  override def nodeExists(nodeId: Long): Boolean = {
    val query = s"MATCH (n) WHERE id(n)=$nodeId RETURN id(n)"
    val result = executor.query(query, emptyCypherMap).asScala.toList
    result.nonEmpty
  }

  override def linkNodes(nodes: Seq[XNode], relationshipNode: XNode): Int = {
    val notLinked = nodes.filter(x => countLinksOnNode(x.dpk) < linksPerNode)
    if (notLinked.nonEmpty) {
      val query = "MATCH (n) WHERE " + notLinked.map(x => "id(n)=" + x.dpk).mkString(" OR ") + " RETURN id(n)"
      val result = executor.query(query, emptyCypherMap).asScala.toList
      val nodeIds = result.map(_.asScala.get("id(n)").get.toString.toLong)
      doLinkNodes(nodeIds, relationshipNode)
    } else 0
  }

  override def linkNodesToAnon(nodes: Seq[XNode], relationshipNode: XNode): Int = {
    val linkNodesFn = (nodeA: Long, nodeB: Long) => addLinkRelationship(nodeA, nodeB, relationshipNode)
    val notLinked = nodes.filter(x => countLinksOnNode(x.dpk) == 0)
    if (notLinked.nonEmpty) {
      val anonId = getAnonId(notLinked.head, linksPerNode, linkNodesFn)
      if (anonId > 0) {
        val query = "MATCH (n) WHERE " + notLinked.filter(_.dpk != anonId).map(x => "id(n)=" + x.dpk).mkString(" OR ") + " RETURN id(n)"
        val result = executor.query(query, emptyCypherMap).asScala.toList
        val nodeIds = result.map(_.asScala.get("id(n)").get.toString.toLong)
        doLinkNodesToAnon(nodeIds, relationshipNode, anonId)
      } else 0
    } else 0
  }

  override def close =
    closeCache
}

/** Also uses nodes cache */
class JdbcFullCacheDbClient(executor: CypherExecutor, linksPerNode: Int) extends JdbcDbClient(executor, linksPerNode) {
  import DbCache._

  def getNodeId(nodeId: Long): Option[Long] =
    nodesCache.get(nodeId)

  override def addNode(node: XNode): Long = {
    val nodeId = super.addNode(node)
    nodesCache.add(nodeId)
    nodeId
  }

  override def nodeExists(nodeId: Long): Boolean =
    getNodeId(nodeId).isDefined

  override def linkNodes(nodes: Seq[XNode], relationshipNode: XNode): Int = {
    val notLinked = nodes.filter(x => countLinksOnNode(x.dpk) < linksPerNode)
    if (notLinked.nonEmpty) {
      val nodeIds = notLinked.flatMap(node => getNodeId(node.dpk)).map(id => id)
      doLinkNodes(nodeIds, relationshipNode)
    } else 0
  }

  override def linkNodesToAnon(nodes: Seq[XNode], relationshipNode: XNode): Int = {
    val linkNodesFn = (nodeA: Long, nodeB: Long) => addLinkRelationship(nodeA, nodeB, relationshipNode)
    val notLinked = nodes.filter(x => countLinksOnNode(x.dpk) == 0)
    if (notLinked.nonEmpty) {
      val anonId = getAnonId(notLinked.head, linksPerNode, linkNodesFn)
      if (anonId > 0) {
        val nodeIds = notLinked.filter(_.dpk != anonId).flatMap(node => getNodeId(node.dpk)).map(id => id)
        doLinkNodesToAnon(nodeIds, relationshipNode, anonId)
      } else 0
    } else 0
  }

}

/** Uses links cache and anon cache */
class DiskDbClient(datadir: File, linksPerNode: Int) extends DbClient with DbClientActions with DbCacheActions {
  import Neo4jToolkit._

  val inserter = BatchInserters.inserter(datadir.getAbsoluteFile, ConfigLoader.load("neo4j-disk.properties").asJava)
  var nodesCount = 0L

  override def countNodes: Long = nodesCount

  override def clear =
    throw new MethodNotSupportedException("clear")

  override def addTriple(triple: XTriple) =
    addSubjectTriples(triple :: Nil)

  override def addSubjectTriples(triples: Seq[XTriple]) =
    doAddSubjectTriples(triples)

  override def addNode(node: XNode): Long = {
    nodesCount += 1
    val (ntype, props) = (nodeTypeString(node), nodeProps(node))
    inserter.createNode(cypherMap(props), DynamicLabel.label(ntype))
  }

  override def addRelationship(nodeA: Long, nodeB: Long, relationshipNode: XNode) = {
    val (rtype, props) = (relationshipType(relationshipNode), nodeProps(relationshipNode))
    inserter.createRelationship(nodeA, nodeB, DynamicRelationshipType.withName(rtype), cypherMap(props))
  }

  override def addLinkRelationship(nodeA: Long, nodeB: Long, relationshipNode: XNode) =
    if (!cacheLinkRelationship(nodeA, nodeB)) addRelationship(nodeA, nodeB, relationshipNode)

  override def nodeExists(nodeId: Long): Boolean =
    inserter.nodeExists(nodeId)

/*
  override def hasRelationship(nodeA: Long, nodeB: Long, relationshipNode: XNode): Boolean = {
    val rtype = relationshipType(relationshipNode)
    val result = inserter.getRelationships(nodeA).asScala.filter(r =>
      r.getType.name == rtype && (r.getStartNode == nodeB || r.getEndNode == nodeB))
    result.nonEmpty
  }
*/

  override def linkNodes(nodes: Seq[XNode], relationshipNode: XNode): Int = {
    val notLinked = nodes.filter(x => countLinksOnNode(x.dpk) < linksPerNode)
    if (notLinked.nonEmpty) {
      val nodeIds = for (x <- notLinked; if nodeExists(x.dpk)) yield x.dpk
      doLinkNodes(nodeIds, relationshipNode)
    } else 0
  }



  override def linkNodesToAnon(nodes: Seq[XNode], relationshipNode: XNode): Int = {
    val linkNodesFn = (nodeA: Long, nodeB: Long) => addLinkRelationship(nodeA, nodeB, relationshipNode)
    val notLinked = nodes.filter(x => countLinksOnNode(x.dpk) == 0)
    if (notLinked.nonEmpty) {
      val anonId = getAnonId(notLinked.head, linksPerNode, linkNodesFn)
      if (anonId > 0) {
        val nodeIds = for (x <- notLinked.filter(_.dpk != anonId); if nodeExists(x.dpk)) yield x.dpk
        doLinkNodesToAnon(nodeIds, relationshipNode, anonId)
      } else 0
    } else 0
  }

  override def close {
    inserter.shutdown
    closeCache
    nodesCount = 0L
  }
}

/** Also uses nodes cache */
class DiskFullCacheDbClient(datadir: File, linksPerNode: Int) extends DiskDbClient(datadir, linksPerNode) {
  import DbCache._

  def getNodeId(nodeId: Long): Option[Long] =
    nodesCache.get(nodeId)

  override def addNode(node: XNode): Long = {
    val nodeId = super.addNode(node)
    nodesCache.add(nodeId)
    nodeId
  }

  override def nodeExists(nodeId: Long): Boolean =
    getNodeId(nodeId).isDefined
}
